package com.walletwise.config.swagger;

import com.walletwise.config.annotations.Generated;
import com.walletwise.domain.Enum.GeneralEnumText;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.tags.Tag;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.List;

@Generated
@Configuration
public class SwaggerConfig {
    @Bean
    public OpenAPI openAPI() {
        String description = "The Personal Budgeting App" + GeneralEnumText.APPLICATION_NAME + "is your comprehensive financial companion,\n" +
                "                                        designed to empower users in managing their finances effectively.\n" +
                "                                        With intuitive features, it allows you to track expenses,\n" +
                "                                        set realistic budgets, and achieve financial goals. Gain insights through visual reports,\n" +
                "                                        receive personalized spending suggestions, and take control of your financial well-being.\n" +
                "                                        Elevate your financial literacy with educational resources integrated within the app.\n" +
                "                                        Start your journey to financial wellness with the Personal Budgeting App today.";
        List<Tag> tags = Arrays.asList(
                new Tag().name("Users").description("Endpoints for some users features."),
                new Tag().name("Authentication").description("Endpoints for some authentication features.")
        );

        return new OpenAPI()
                .info(new Info()
                        .title(GeneralEnumText.APPLICATION_NAME.getValue())
                        .version(GeneralEnumText.APPLICATION_VERSION.getValue())
                        .description(description))
                .tags(tags);

    }
}
