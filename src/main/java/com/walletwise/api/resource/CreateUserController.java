package com.walletwise.api.resource;

import com.walletwise.api.validation.ValidationBuilder;
import com.walletwise.api.validation.validator.contract.IValidator;
import com.walletwise.domain.dto.CreateUserRequest;
import com.walletwise.domain.exception.BusinessException;
import com.walletwise.domain.model.UserAccount;
import com.walletwise.gateway.helper.http.Response;
import com.walletwise.service.contract.ICreateUserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@Tag(name = "Users")
@RequiredArgsConstructor
@RequestMapping("/users/create_user")
public class CreateUserController extends AbstractController<CreateUserRequest> {
    private final ICreateUserService service;

    @Override
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(summary = "Register user operation")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Returns the user token"),
            @ApiResponse(responseCode = "400", description = "Bad request happened"),
            @ApiResponse(responseCode = "500", description = "Internal server error occurred"),
    })
    public ResponseEntity<Response> perform(CreateUserRequest request) {
        Response response;
        ResponseEntity<Response> responseEntity;

        String error = this.validate(request);
        if (error != null) {
            response = Response.builder().statusCode(HttpStatus.BAD_REQUEST.value()).body(error).build();
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }

        try {
            UserAccount userAccount = this.service.create(request);
            response = Response.builder().statusCode(HttpStatus.CREATED.value()).body(userAccount).build();
            responseEntity = new ResponseEntity<>(response, HttpStatus.CREATED);

        } catch (BusinessException ex) {
            response = Response.builder().statusCode(HttpStatus.BAD_REQUEST.value()).body(ex.getMessage()).build();
            responseEntity = new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        } catch (Exception ex) {
            response = Response.builder().statusCode(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(ex.getMessage()).build();
            responseEntity = new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @Override
    public List<IValidator> buildValidators(CreateUserRequest request) {
        List<IValidator> validators = new ArrayList<>();
        validators.addAll(ValidationBuilder.of("First name", request.getFirstName()).required().build());
        validators.addAll(ValidationBuilder.of("Last name", request.getLastName()).required().build());
        validators.addAll(ValidationBuilder.of("username", request.getUsername()).required().username().build());
        validators.addAll(ValidationBuilder.of("email", request.getEmail()).required().email().build());
        validators.addAll(ValidationBuilder.of("password", request.getPassword()).required().password().build());
        return validators;
    }
}
