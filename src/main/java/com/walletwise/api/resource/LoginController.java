package com.walletwise.api.resource;

import com.walletwise.api.validation.ValidationBuilder;
import com.walletwise.api.validation.validator.contract.IValidator;
import com.walletwise.domain.dto.LoginRequest;
import com.walletwise.domain.exception.BusinessException;
import com.walletwise.domain.model.UserAccount;
import com.walletwise.gateway.helper.http.Response;
import com.walletwise.service.contract.ILoginService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequiredArgsConstructor
@Tag(name = "Authentication")
@RequestMapping("/auth/login")
public class LoginController extends AbstractController<LoginRequest> {
    private final ILoginService service;

    @Override
    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Authentication operation")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returns the user token"),
            @ApiResponse(responseCode = "400", description = "Bad request happened"),
            @ApiResponse(responseCode = "500", description = "Internal server error occurred"),
    })
    public ResponseEntity<Response> perform(LoginRequest request) {
        Response response;
        ResponseEntity<Response> responseEntity;

        String error = this.validate(request);
        if (error != null) {
            response = Response.builder().statusCode(HttpStatus.BAD_REQUEST.value()).body(error).build();
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }

        try {
            UserAccount userAccount = this.service.login(request);
            response = Response.builder().statusCode(HttpStatus.OK.value()).body(userAccount).build();
            responseEntity = new ResponseEntity<>(response, HttpStatus.OK);
        } catch (BusinessException | AuthenticationException ex) {
            if (ex instanceof AuthenticationException) {
                response = Response.builder().statusCode(HttpStatus.UNAUTHORIZED.value()).body(ex.getMessage()).build();
                responseEntity = new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
            } else {
                response = Response.builder().statusCode(HttpStatus.BAD_REQUEST.value()).body(ex.getMessage()).build();
                responseEntity = new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception ex) {
            response = Response.builder().statusCode(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(ex.getMessage()).build();
            responseEntity = new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @Override
    public List<IValidator> buildValidators(LoginRequest request) {
        List<IValidator> validators = new ArrayList<>();
        validators.addAll(ValidationBuilder.of("Username or Email", request.getUsernameOrEmail()).required().build());
        validators.addAll(ValidationBuilder.of("password", request.getPassword()).required().build());
        return validators;
    }
}
