package com.walletwise.api.resource;

import com.walletwise.api.validation.ValidationComposite;
import com.walletwise.api.validation.validator.contract.IValidator;
import com.walletwise.gateway.helper.http.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

public abstract class AbstractController<E> {
    public abstract ResponseEntity<Response> perform(@RequestBody E request);

    public abstract List<IValidator> buildValidators(E request);

    protected String validate(E request) {
        List<IValidator> validators = this.buildValidators(request);
        return new ValidationComposite(validators).validate();
    }
}
