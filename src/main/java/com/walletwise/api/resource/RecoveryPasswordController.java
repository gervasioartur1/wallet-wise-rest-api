package com.walletwise.api.resource;

import com.walletwise.api.validation.ValidationBuilder;
import com.walletwise.api.validation.validator.contract.IValidator;
import com.walletwise.domain.dto.RecoveryPasswordRequest;
import com.walletwise.domain.exception.BusinessException;
import com.walletwise.gateway.helper.http.Response;
import com.walletwise.service.contract.IRecoveryPasswordService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequiredArgsConstructor
@Tag(name = "Authentication")
@RequestMapping("/auth/recovery_password")
public class RecoveryPasswordController extends AbstractController<RecoveryPasswordRequest> {
    private final IRecoveryPasswordService service;

    @Override
    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Recovery password operation")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returns success message"),
            @ApiResponse(responseCode = "400", description = "Bad request happened"),
            @ApiResponse(responseCode = "500", description = "Internal server error occurred"),
    })
    public ResponseEntity<Response> perform(RecoveryPasswordRequest request) {
        Response response;
        ResponseEntity<Response> responseEntity;

        String error = this.validate(request);
        if (error != null) {
            response = Response.builder().statusCode(HttpStatus.BAD_REQUEST.value()).body(error).build();
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }

        try {
            this.service.recovery(request);
            response = Response.builder().statusCode(HttpStatus.OK.value()).body("We´ve send instruction to recopy your password in the email : " + request.getEmail()).build();
            responseEntity = new ResponseEntity<>(response, HttpStatus.OK);
        } catch (BusinessException ex) {
            response = Response.builder().statusCode(HttpStatus.BAD_REQUEST.value()).body(ex.getMessage()).build();
            responseEntity = new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        } catch (Exception ex) {
            response = Response.builder().statusCode(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(ex.getMessage()).build();
            responseEntity = new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return responseEntity;
    }

    @Override
    public List<IValidator> buildValidators(RecoveryPasswordRequest request) {
        return new ArrayList<>(ValidationBuilder.of("Username or Email", request.getEmail()).required().build());
    }
}
