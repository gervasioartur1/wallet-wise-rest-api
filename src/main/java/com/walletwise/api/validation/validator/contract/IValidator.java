package com.walletwise.api.validation.validator.contract;

public interface IValidator {
    String validate();
}
