package com.walletwise.api.validation.validator;


import com.walletwise.api.validation.validator.contract.IValidator;
import com.walletwise.config.annotations.Generated;

@Generated
public abstract class AbstractValidator implements IValidator {
    protected String fieldName;
    protected Object fieldValue;

    @Override
    public String validate() {
        return null;
    }
}
