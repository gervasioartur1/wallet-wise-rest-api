package com.walletwise.api.validation;

import com.walletwise.api.validation.validator.EmailFieldValidator;
import com.walletwise.api.validation.validator.PasswordFieldValidator;
import com.walletwise.api.validation.validator.RequiredFieldValidator;
import com.walletwise.api.validation.validator.UsernameFieldValidator;
import com.walletwise.api.validation.validator.contract.IValidator;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class ValidationBuilder {
    private final List<IValidator> validators = new ArrayList<>();
    private String fieldName;
    private Object fieldValue;

    private ValidationBuilder(String fieldName, Object fieldValue) {
        this.fieldName = fieldName;
        this.fieldValue = fieldValue;
    }

    public static ValidationBuilder of(String fieldName, Object fieldValue) {
        return new ValidationBuilder(fieldName, fieldValue);
    }

    public ValidationBuilder required() {
        this.validators.add(new RequiredFieldValidator(this.fieldName, this.fieldValue));
        return this;
    }

    public ValidationBuilder email() {
        this.validators.add(new EmailFieldValidator(this.fieldName, this.fieldValue));
        return this;
    }

    public ValidationBuilder password() {
        this.validators.add(new PasswordFieldValidator(this.fieldName, this.fieldValue));
        return this;
    }

    public ValidationBuilder username() {
        this.validators.add(new UsernameFieldValidator(this.fieldValue));
        return this;
    }

    public List<IValidator> build() {
        return this.validators;
    }
}
