package com.walletwise.gateway.repository;

import com.walletwise.domain.entity.UserValidationCode;
import lombok.NonNull;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface IUserValidationCodeRepository extends JpaRepository<UserValidationCode, UUID> {
    Optional<UserValidationCode> findByUserId(UUID userId);
    void deleteById(@NonNull UUID id);
}
