package com.walletwise.gateway.Utils;

import lombok.Getter;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.LocalDateTime;

@Getter
@Component
public class CalculateDifferenceBetweenDates {
    public Duration calculate(LocalDateTime registeredDate, LocalDateTime currentDate) {
        return Duration.between(registeredDate, currentDate);
    }
}
