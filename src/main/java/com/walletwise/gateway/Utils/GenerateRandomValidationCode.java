package com.walletwise.gateway.Utils;

import com.walletwise.domain.Enum.GeneralEnumInt;
import lombok.Getter;
import org.springframework.stereotype.Component;

import java.util.Random;

@Getter
@Component
public class GenerateRandomValidationCode {
    private final int validationCode;

    private GenerateRandomValidationCode() {
        Random random = new Random();
        this.validationCode = random
                .nextInt(GeneralEnumInt.MAX_VALIDATION_CODE_LENGTH.getValue()) + GeneralEnumInt.MIN_VALIDATION_CODE_LENGTH.getValue();
    }
}
