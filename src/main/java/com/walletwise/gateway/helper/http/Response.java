package com.walletwise.gateway.helper.http;

import com.walletwise.config.annotations.Generated;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@Generated
@NoArgsConstructor
@AllArgsConstructor
public class Response {
    private int statusCode;
    private Object body;
}
