package com.walletwise.gateway.helper.token;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
@RequiredArgsConstructor
public class GenerateToken {
    private final CreateToken createToken;

    public String generate(String username) {
        Map<String, Object> claims = new HashMap<>();
        return this.createToken.create(claims, username);
    }
}
