package com.walletwise.gateway.helper.token;

import com.walletwise.domain.Enum.GeneralEnumInt;
import com.walletwise.gateway.helper.security.SingKey;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Map;

@Component
public class CreateToken {
    public String create(Map<String, Object> claims, String username) {
        return io.jsonwebtoken.Jwts.builder()
                .setClaims(claims)
                .setSubject(username)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + GeneralEnumInt.JWT_TOKEN_EXPIRATION.getValue()))
                .signWith(SingKey.getSignKey(), SignatureAlgorithm.HS256).compact();
    }
}
