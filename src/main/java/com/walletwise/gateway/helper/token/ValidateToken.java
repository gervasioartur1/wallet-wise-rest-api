package com.walletwise.gateway.helper.token;

import com.walletwise.gateway.helper.security.SingKey;
import io.jsonwebtoken.Jwts;
import org.springframework.stereotype.Component;

@Component
public class ValidateToken {
    public void validate(final String token) {
        Jwts.parserBuilder().setSigningKey(SingKey.getSignKey()).build().parseClaimsJws(token);
    }
}
