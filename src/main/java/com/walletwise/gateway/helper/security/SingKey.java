package com.walletwise.gateway.helper.security;

import com.walletwise.domain.Enum.GeneralEnumText;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;

import java.security.Key;

public class SingKey {
    public static Key getSignKey() {
        byte[] keyBytes = Decoders
                .BASE64.decode(GeneralEnumText.SECRET.getValue());
        return Keys.hmacShaKeyFor(keyBytes);
    }
}
