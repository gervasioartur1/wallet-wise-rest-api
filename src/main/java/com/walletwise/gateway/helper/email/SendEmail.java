package com.walletwise.gateway.helper.email;

import com.walletwise.domain.Enum.GeneralEnumText;
import com.walletwise.domain.dto.SendEmailRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Component;

import java.rmi.UnexpectedException;

@Component
@RequiredArgsConstructor
public class SendEmail {
    private final JavaMailSenderImpl mailSender;

    public void send(SendEmailRequest request) throws UnexpectedException {
        try {
            SimpleMailMessage message = new SimpleMailMessage();
            String fromEmail = GeneralEnumText.EMAIL_SENDER.getValue();
            message.setFrom(fromEmail);
            message.setTo(request.getReceiver());
            message.setSubject(request.getSubject());
            message.setText(request.getBody());
            mailSender.send(message);
        } catch (Exception e) {
            throw new UnexpectedException(e.getMessage());
        }
    }

}
