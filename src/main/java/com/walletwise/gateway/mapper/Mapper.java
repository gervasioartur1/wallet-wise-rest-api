package com.walletwise.gateway.mapper;

import com.walletwise.config.annotations.Generated;
import com.walletwise.domain.dto.CreateUserRequest;
import com.walletwise.domain.entity.User;

@Generated
public class Mapper {
    public static User createUserRequestToUserMapper(CreateUserRequest request) {
        return User
                .builder()
                .firstName(request.getFirstName())
                .lastName(request.getLastName())
                .username(request.getUsername())
                .email(request.getEmail())
                .password(request.getPassword())
                .active(true)
                .role("USER")
                .build();
    }
}
