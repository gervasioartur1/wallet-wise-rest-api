package com.walletwise.domain.Enum;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum GeneralEnumInt {
    EXPIRE_TIME_OF_VALIDATION_CODE(15),
    MIN_VALIDATION_CODE_LENGTH(100000),
    MAX_VALIDATION_CODE_LENGTH(900000),
    JWT_TOKEN_EXPIRATION(1000 * 60 * 30);
    private final int value;
}
