package com.walletwise.domain.Enum;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum GeneralEnumText {
    APPLICATION_NAME("WALLET-WISE"),
    APPLICATION_VERSION("V1"),
    EMAIL_SENDER("gervasioarthur@gmail.com"),
    SECRET("5367566B59703373367639792F423F4528482B4D6251655468576D5A71347437"),
    PASSWORD_REGEX_EXPRESSION("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{6,32}$"),
    EMAIL_PASSWORD_EXPRESSION("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}$");
    private final String value;
}
