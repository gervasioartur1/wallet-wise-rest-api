package com.walletwise.domain.model;

import com.walletwise.config.annotations.Generated;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@Generated
@NoArgsConstructor
@AllArgsConstructor
public class UserAccount {
    private String token;
}
