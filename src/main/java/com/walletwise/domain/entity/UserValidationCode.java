package com.walletwise.domain.entity;

import com.walletwise.config.annotations.Generated;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Entity
@Builder
@Generated
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "t_user_validation_code", schema = "walletwise")
public class UserValidationCode {
    @Id
    @Column(length = 16)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @Column(nullable = false)
    private UUID userId;

    @OneToOne
    @JoinColumn(name = "fk_user_id", referencedColumnName = "id", nullable = false)
    private User user;

    @Column(nullable = false)
    private String code;

    @Column(nullable = false)
    private boolean validated = false;

    @CreationTimestamp
    @Column(nullable = false)
    private LocalDateTime createdAt;
}
