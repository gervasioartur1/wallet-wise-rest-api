package com.walletwise.service.impl;

import com.walletwise.domain.dto.CreateNewPasswordRequest;
import com.walletwise.domain.entity.User;
import com.walletwise.domain.entity.UserValidationCode;
import com.walletwise.domain.exception.BusinessException;
import com.walletwise.gateway.repository.IUserRepository;
import com.walletwise.gateway.repository.IUserValidationCodeRepository;
import com.walletwise.service.contract.ICreateNewPasswordService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Transactional
@RequiredArgsConstructor
public class CreateNewPasswordService implements ICreateNewPasswordService {
    private final IUserRepository repository;
    private final IUserValidationCodeRepository userValidationCodeRepository;
    private final PasswordEncoder encoder;

    @Override
    public void create(CreateNewPasswordRequest request) {
        Optional<User> userResult = this.repository.findByEmail(request.getEmail());

        if (userResult.isEmpty())
            throw new BusinessException("Can´t associate the email to any account.");

        Optional<UserValidationCode> userValidationCode = this.userValidationCodeRepository
                .findByUserId(userResult.get().getId());

        if (userValidationCode.isEmpty())
            throw new BusinessException("Can´t process the information, make sure you´ve reset you password to user this page.");

        if (!userValidationCode.get().isValidated())
            throw new BusinessException("Please,Validate the code send on your email to create a new password!");

        String encodedPassword = encoder.encode(request.getPassword());
        User user = userResult.get();
        user.setPassword(encodedPassword);
        user.setActive(true);
        this.repository.save(user);

        this.userValidationCodeRepository.deleteById(userValidationCode.get().getId());
    }
}
