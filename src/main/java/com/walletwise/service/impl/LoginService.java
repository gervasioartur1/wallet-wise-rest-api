package com.walletwise.service.impl;

import com.walletwise.domain.dto.LoginRequest;
import com.walletwise.domain.entity.User;
import com.walletwise.domain.exception.BusinessException;
import com.walletwise.domain.model.UserAccount;
import com.walletwise.gateway.helper.token.GenerateToken;
import com.walletwise.gateway.repository.IUserRepository;
import com.walletwise.service.contract.ILoginService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class LoginService implements ILoginService {
    private final IUserRepository repository;
    private final AuthenticationManager authenticationManager;
    private final GenerateToken generateToken;

    @Override
    public UserAccount login(LoginRequest request) {
        Optional<User> user;

        if (request.getUsernameOrEmail().contains("@")) {
            user = this.repository.findByEmail(request.getUsernameOrEmail());
            if (user.isEmpty())
                throw new BusinessException("The email: " + request.getUsernameOrEmail() + ", is not associated with any account! Try again with correct credentials or Registry yourself.");
        } else {
            user = this.repository.findByUsername(request.getUsernameOrEmail());
            if (user.isEmpty())
                throw new BusinessException("The username: " + request.getUsernameOrEmail() + ", is not associated with any account! Try again with correct credentials or Registry yourself.");
        }

        if (!user.get().isActive())
            throw new BusinessException("The account is not activated! please verify your email get instructions to active tour account.");

        Authentication authentication = this.authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(user.get().getUsername(), request.getPassword()));

        if (!authentication.isAuthenticated())
            throw new BusinessException("Invalid username or password!");

        String token = this.generateToken.generate(user.get().getUsername());
        return UserAccount.builder().token(token).build();
    }
}
