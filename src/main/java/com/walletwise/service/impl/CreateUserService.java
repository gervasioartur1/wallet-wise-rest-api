package com.walletwise.service.impl;

import com.walletwise.domain.dto.CreateUserRequest;
import com.walletwise.domain.entity.User;
import com.walletwise.domain.exception.BusinessException;
import com.walletwise.domain.model.UserAccount;
import com.walletwise.gateway.helper.token.GenerateToken;
import com.walletwise.gateway.mapper.Mapper;
import com.walletwise.gateway.repository.IUserRepository;
import com.walletwise.service.contract.ICreateUserService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@Transactional
@RequiredArgsConstructor
public class CreateUserService implements ICreateUserService {
    private final IUserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final GenerateToken generateToken;
    private final AuthenticationManager authenticationManager;

    @Override
    public UserAccount create(CreateUserRequest request) {
        if (this.userRepository.findByEmail(request.getEmail()).isPresent())
            throw new BusinessException("We've found an account with email: " + request.getEmail() + "! Try to login to the account.");

        if (this.userRepository.findByUsername(request.getUsername()).isPresent())
            throw new BusinessException("We've found an account with username: " + request.getUsername() + "! Try to login to the account.");

        String notEncodedPassword = request.getPassword();
        String encodedPassword = this.passwordEncoder.encode(request.getPassword());
        request.setPassword(encodedPassword);

        User user = Mapper.createUserRequestToUserMapper(request);
        user = this.userRepository.save(user);

        this.authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(user.getUsername(), notEncodedPassword));
        String token = this.generateToken.generate(user.getUsername());
        return UserAccount.builder().token(token).build();
    }
}
