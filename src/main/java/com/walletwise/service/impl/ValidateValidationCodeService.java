package com.walletwise.service.impl;

import com.walletwise.domain.Enum.GeneralEnumInt;
import com.walletwise.domain.dto.ValidateValidationCodeRequest;
import com.walletwise.domain.entity.User;
import com.walletwise.domain.entity.UserValidationCode;
import com.walletwise.domain.exception.BusinessException;
import com.walletwise.gateway.Utils.CalculateDifferenceBetweenDates;
import com.walletwise.gateway.repository.IUserRepository;
import com.walletwise.gateway.repository.IUserValidationCodeRepository;
import com.walletwise.service.contract.IValidateValidationCodeService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Optional;

@Service
@Transactional
@RequiredArgsConstructor
public class ValidateValidationCodeService implements IValidateValidationCodeService {
    private final IUserRepository repository;
    private final IUserValidationCodeRepository userValidationCodeRepository;
    private final PasswordEncoder encoder;
    private final CalculateDifferenceBetweenDates calculateDifferenceBetweenDates;

    @Override
    public void validate(ValidateValidationCodeRequest request) {
        Optional<User> user = repository.findByEmail(request.getEmail());
        if (user.isEmpty())
            throw new BusinessException("Can´t associate the email to any account.");

        if (user.get().isActive())
            throw new BusinessException("These account is already active. Please try to login with your account.");

        Optional<UserValidationCode> userValidationCode = this.userValidationCodeRepository.findByUserId(user.get().getId());

        if (userValidationCode.isEmpty())
            throw new BusinessException("Invalid validation code. Please try again.");

        if (!this.encoder.matches(request.getValidationCode(), userValidationCode.get().getCode()))
            throw new BusinessException("Invalid validation code. Please try again.");

        Duration duration = this.calculateDifferenceBetweenDates.calculate(userValidationCode.get().getCreatedAt(), LocalDateTime.now());

        if (duration.toMinutes() >= GeneralEnumInt.EXPIRE_TIME_OF_VALIDATION_CODE.getValue())
            throw new BusinessException("The validation code you´ve entered has expired.");

        userValidationCode.get().setValidated(true);
        this.userValidationCodeRepository.save(userValidationCode.get());
    }
}
