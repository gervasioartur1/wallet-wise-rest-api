package com.walletwise.service.impl;

import com.walletwise.domain.dto.RecoveryPasswordRequest;
import com.walletwise.domain.dto.SendEmailRequest;
import com.walletwise.domain.entity.User;
import com.walletwise.domain.entity.UserValidationCode;
import com.walletwise.domain.exception.BusinessException;
import com.walletwise.gateway.Utils.GenerateRandomValidationCode;
import com.walletwise.gateway.helper.email.SendEmail;
import com.walletwise.gateway.repository.IUserRepository;
import com.walletwise.gateway.repository.IUserValidationCodeRepository;
import com.walletwise.service.contract.IRecoveryPasswordService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.rmi.UnexpectedException;
import java.time.LocalDateTime;
import java.util.Optional;

@Service
@Transactional
@RequiredArgsConstructor
public class RecoveryPasswordService implements IRecoveryPasswordService {
    private final IUserRepository repository;
    private final IUserValidationCodeRepository userValidationCodeRepository;
    private final GenerateRandomValidationCode validationCodeGenerator;
    private final SendEmail sendEmail;
    private final PasswordEncoder encoder;

    @Override
    public void recovery(RecoveryPasswordRequest request) throws UnexpectedException {
        Optional<User> userResult = repository.findByEmail(request.getEmail());
        if (userResult.isEmpty())
            throw new BusinessException("Could´t find your account, make sure you entered the correct credentials!");

        Optional<UserValidationCode> userValidationCodeResult = this.userValidationCodeRepository.findByUserId(userResult.get().getId());
        int validationCode = this.validationCodeGenerator.getValidationCode();
        String encodedValidationCode = this.encoder.encode(String.valueOf(validationCode));

        UserValidationCode toSaveUserValidationCode;

        if (userValidationCodeResult.isEmpty()) {
            toSaveUserValidationCode = UserValidationCode
                    .builder()
                    .userId(userResult.get().getId())
                    .code(encodedValidationCode)
                    .build();
        } else {
            toSaveUserValidationCode = userValidationCodeResult.get();
            toSaveUserValidationCode.setCode(encodedValidationCode);
            LocalDateTime rightNow = LocalDateTime.now();
            toSaveUserValidationCode.setCreatedAt(rightNow);
            toSaveUserValidationCode.setValidated(false);
        }

        toSaveUserValidationCode.setUser(userResult.get());
        this.userValidationCodeRepository.save(toSaveUserValidationCode);

        User user = userResult.get();
        user.setActive(false);
        user = this.repository.save(user);

        SendEmailRequest sendEmailRequest = SendEmailRequest
                .builder()
                .receiver(user.getEmail())
                .subject("Recovery Password")
                .body("Recovery your password using the follow validation code: \n" + validationCode)
                .build();
        this.sendEmail.send(sendEmailRequest);
    }
}
