package com.walletwise.service.impl;

import com.walletwise.domain.entity.CustomUserDetails;
import com.walletwise.domain.entity.User;
import com.walletwise.gateway.repository.IUserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CustomUserDetailsService implements UserDetailsService {
    private final IUserRepository repository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> userCredential = this.repository.findByUsername(username);
        return userCredential
                .map(CustomUserDetails::new)
                .orElseThrow(
                        () -> new UsernameNotFoundException("The username: " + username + ", is not associated with any account! Try again with correct credentials or Registry yourself."));
    }
}