package com.walletwise.service.contract;

import com.walletwise.domain.dto.RecoveryPasswordRequest;

import java.rmi.UnexpectedException;

public interface IRecoveryPasswordService {
    void recovery(RecoveryPasswordRequest request) throws UnexpectedException;
}
