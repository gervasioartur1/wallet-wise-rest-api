package com.walletwise.service.contract;

import com.walletwise.domain.dto.ValidateValidationCodeRequest;

public interface IValidateValidationCodeService {
    void validate(ValidateValidationCodeRequest request);
}
