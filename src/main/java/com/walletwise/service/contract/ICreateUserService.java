package com.walletwise.service.contract;

import com.walletwise.domain.dto.CreateUserRequest;
import com.walletwise.domain.model.UserAccount;

public interface ICreateUserService {
    UserAccount create(CreateUserRequest request);
}
