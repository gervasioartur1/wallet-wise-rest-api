package com.walletwise.service.contract;

import com.walletwise.domain.dto.LoginRequest;
import com.walletwise.domain.model.UserAccount;

public interface ILoginService {
    UserAccount login(LoginRequest request);
}
