package com.walletwise.service.contract;

import com.walletwise.domain.dto.CreateNewPasswordRequest;

public interface ICreateNewPasswordService {
    void create(CreateNewPasswordRequest request);
}
