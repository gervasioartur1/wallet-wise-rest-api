DROP TABLE t_user CASCADE;

CREATE SCHEMA walletwise;

CREATE TABLE walletwise.t_user
(
    id         UUID         NOT NULL,
    first_name VARCHAR(255) NOT NULL,
    last_name  VARCHAR(255) NOT NULL,
    username   VARCHAR(255) NOT NULL,
    email      VARCHAR(255) NOT NULL,
    password   VARCHAR(255) NOT NULL,
    role       VARCHAR(255) NOT NULL,
    CONSTRAINT pk_t_user PRIMARY KEY (id)
);