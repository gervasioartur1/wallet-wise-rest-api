ALTER TABLE walletwise.t_user_validation_code
    ADD validated BOOLEAN;

ALTER TABLE walletwise.t_user_validation_code
    ALTER COLUMN validated SET NOT NULL;

ALTER TABLE walletwise.t_user_validation_code
    ADD CONSTRAINT uc_t_user_validation_code_fk_user UNIQUE (fk_user_id);