CREATE TABLE t_user
(
    id         CHAR(36) NOT NULL,
    email      VARCHAR(255),
    first_name VARCHAR(255),
    last_name  VARCHAR(255),
    password   VARCHAR(255),
    role       VARCHAR(255),
    username   VARCHAR(255),
    CONSTRAINT t_user_pkey PRIMARY KEY (id)
);