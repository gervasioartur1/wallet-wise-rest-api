CREATE TABLE walletwise.t_user_validation_code
(
    id         UUID                        NOT NULL,
    user_id    UUID                        NOT NULL,
    fk_user_id UUID                        NOT NULL,
    code       VARCHAR(255)                NOT NULL,
    created_at TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    CONSTRAINT pk_t_user_validation_code PRIMARY KEY (id)
);

ALTER TABLE walletwise.t_user_validation_code
    ADD CONSTRAINT FK_T_USER_VALIDATION_CODE_ON_FK_USER FOREIGN KEY (fk_user_id) REFERENCES walletwise.t_user (id);

ALTER TABLE walletwise.t_user
    ALTER COLUMN created_at SET NOT NULL;

ALTER TABLE walletwise.t_user
    ALTER COLUMN updated_at SET NOT NULL;