ALTER TABLE walletwise.t_user
    ADD active BOOLEAN;

ALTER TABLE walletwise.t_user
    ALTER COLUMN active SET NOT NULL;