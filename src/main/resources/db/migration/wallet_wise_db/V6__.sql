ALTER TABLE walletwise.t_user
    ALTER COLUMN created_at SET NOT NULL;

ALTER TABLE walletwise.t_user
    ALTER COLUMN updated_at SET NOT NULL;