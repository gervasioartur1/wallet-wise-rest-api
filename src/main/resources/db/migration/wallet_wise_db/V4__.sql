ALTER TABLE walletwise.t_user
    ADD CONSTRAINT uc_t_user_email UNIQUE (email);

ALTER TABLE walletwise.t_user
    ADD CONSTRAINT uc_t_user_username UNIQUE (username);

alter table if exists walletwise.t_user
    add column created_at timestamp(6);

alter table if exists walletwise.t_user
    add column updated_at timestamp(6);