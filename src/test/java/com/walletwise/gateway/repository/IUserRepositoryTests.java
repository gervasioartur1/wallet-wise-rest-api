package com.walletwise.gateway.repository;

import com.walletwise.domain.entity.User;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
class IUserRepositoryTests {
    @Autowired
    TestEntityManager entityManager;

    @Autowired
    IUserRepository userRepository;

    private User userFactory() {
        return User
                .builder()
                .firstName("any_first_name")
                .lastName("any_last_name")
                .username("any_username")
                .email("any_email")
                .password("any_password")
                .active(true)
                .role("any_role")
                .build();
    }

    @Test
    @DisplayName("Should return empty if user email not found")
    void shouldReturnEmptyIfUserEmailNotFound() {
        String email = "any_email";
        Optional<User> user = this.userRepository.findByEmail(email);
        assertThat(user).isEmpty();
    }

    @Test
    @DisplayName("Should return optional of user  email exists")
    void shouldReturnOptionalOfUserEmailExists() {
        String email = "any_email";
        User user = this.userFactory();
        entityManager.persist(user);
        Optional<User> userResult = this.userRepository.findByEmail(email);
        assertThat(userResult.isPresent()).isTrue();
        assertThat(userResult.get().getCreatedAt()).isNotNull();
        assertThat(userResult.get().getUpdatedAt()).isNotNull();
    }

    @Test
    @DisplayName("Should return empty if user username not found")
    void shouldReturnEmptyIfUserUsernameNotFound() {
        String email = "any_username";
        Optional<User> user = this.userRepository.findByUsername(email);
        assertThat(user).isEmpty();
    }

    @Test
    @DisplayName("Should return optional of user  username exists")
    void shouldReturnOptionalOfUserUsernameExists() {
        String email = "any_username";
        User user = this.userFactory();
        entityManager.persist(user);
        Optional<User> userResult = this.userRepository.findByUsername(email);
        assertThat(userResult.isPresent()).isTrue();
        assertThat(userResult.get().getCreatedAt()).isNotNull();
        assertThat(userResult.get().getUpdatedAt()).isNotNull();
    }

    @Test
    @DisplayName("Should  save user")
    void shouldSaveUser() {
        User user = this.userFactory();
        User savedUser = this.userRepository.save(user);
        assertThat(savedUser.getId()).isNotNull();
    }
}
