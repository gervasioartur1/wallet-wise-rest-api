package com.walletwise.service.impl;

import com.walletwise.domain.dto.CreateUserRequest;
import com.walletwise.domain.entity.User;
import com.walletwise.domain.model.UserAccount;
import com.walletwise.gateway.helper.token.GenerateToken;
import com.walletwise.gateway.repository.IUserRepository;
import com.walletwise.service.contract.ICreateUserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Mockito.*;

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
class CreateUserServiceTests {
    ICreateUserService userService;
    @MockBean
    IUserRepository userRepository;
    @MockBean
    PasswordEncoder passwordEncoder;
    @MockBean
    GenerateToken generateToken;
    @MockBean
    AuthenticationManager authenticationManager;

    @BeforeEach
    void setup() {
        this.userService = new CreateUserService
                (userRepository, passwordEncoder, generateToken, authenticationManager);
    }


    @Test
    @DisplayName("Should throw businessException if email is already registered")
    void shouldThrowBusinessExceptionIfEmailIsAlreadyRegistered() {
        User savedUser = mocks.userFactory();
        User toSaveUser = mocks.toSaveUserFactory();
        CreateUserRequest request = mocks.createUserRequestFactory();

        when(userRepository.findByEmail(request.getEmail())).thenReturn(Optional.of(savedUser));

        Throwable exception = catchThrowable(() -> this.userService.create(request));

        verify(userRepository, times(1)).findByEmail(request.getEmail());
        verify(userRepository, times(0)).findByUsername(request.getUsername());
        verify(userRepository, times(0)).save(toSaveUser);
        assertThat(exception.getMessage()).isEqualTo("We've found an account with email: " + request.getEmail() + "! Try to login to the account.");
    }

    @Test
    @DisplayName("Should throw businessException if the username is already in use")
    void shouldThrowBusinessExceptionIfUsernameAlreadyInUse() {
        User savedUser = mocks.userFactory();
        User toSaveUser = mocks.toSaveUserFactory();
        CreateUserRequest request = mocks.createUserRequestFactory();

        when(userRepository.findByEmail(request.getEmail())).thenReturn(Optional.empty());
        when(userRepository.findByUsername(request.getUsername())).thenReturn(Optional.of(savedUser));


        Throwable exception = catchThrowable(() -> this.userService.create(request));

        verify(userRepository, times(1)).findByEmail(request.getEmail());
        verify(userRepository, times(1)).findByUsername(request.getUsername());
        verify(userRepository, times(0)).save(toSaveUser);
        assertThat(exception.getMessage()).isEqualTo("We've found an account with username: " + request.getUsername() + "! Try to login to the account.");
    }

    @Test
    @DisplayName("should save user information")
    void shouldSaveUserInformation() {
        User savedUser = mocks.userFactory();
        User toSaveUser = mocks.toSaveUserFactory();
        CreateUserRequest request = mocks.createUserRequestFactory();

        String encodedPassword = UUID.randomUUID().toString();
        String notEncodedPassword = request.getPassword();
        toSaveUser.setPassword(encodedPassword);

        when(userRepository.findByEmail(request.getEmail())).thenReturn(Optional.empty());
        when(userRepository.findByUsername(request.getUsername())).thenReturn(Optional.empty());
        when(passwordEncoder.encode(notEncodedPassword)).thenReturn(encodedPassword);
        when(userRepository.save(toSaveUser)).thenReturn(savedUser);

        this.userService.create(request);

        verify(userRepository, times(1)).findByEmail(request.getEmail());
        verify(userRepository, times(1)).findByUsername(request.getUsername());
        verify(passwordEncoder, times(1)).encode(notEncodedPassword);
        verify(userRepository, times(1)).save(toSaveUser);
    }

    @Test
    @DisplayName("should return userAccount on success")
    void shouldReturnUserAccountOnSuccess() {
        Authentication authentication = mocks.authenticationFactory(true);
        User savedUser = mocks.userFactory();
        User toSaveUser = mocks.toSaveUserFactory();
        CreateUserRequest request = mocks.createUserRequestFactory();

        String encodedPassword = UUID.randomUUID().toString();
        String notEncodedPassword = request.getPassword();
        String token = UUID.randomUUID().toString();

        toSaveUser.setPassword(encodedPassword);

        when(userRepository.findByEmail(request.getEmail())).thenReturn(Optional.empty());
        when(userRepository.findByUsername(request.getUsername())).thenReturn(Optional.empty());
        when(passwordEncoder.encode(notEncodedPassword)).thenReturn(encodedPassword);
        when(userRepository.save(toSaveUser)).thenReturn(savedUser);
        when(this.authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(savedUser.getUsername(), savedUser.getPassword()))).thenReturn(authentication);
        when(generateToken.generate(savedUser.getUsername())).thenReturn(token);

        UserAccount userAccount = this.userService.create(request);

        verify(userRepository, times(1)).findByEmail(request.getEmail());
        verify(userRepository, times(1)).findByUsername(request.getUsername());
        verify(passwordEncoder, times(1)).encode(notEncodedPassword);
        verify(userRepository, times(1)).save(toSaveUser);
        verify(this.authenticationManager, atLeast(1)).authenticate(new UsernamePasswordAuthenticationToken(savedUser.getUsername(), savedUser.getPassword()));
        verify(generateToken, times(1)).generate(savedUser.getUsername());
        assertThat(userAccount.getToken()).isEqualTo(token);
    }
}
