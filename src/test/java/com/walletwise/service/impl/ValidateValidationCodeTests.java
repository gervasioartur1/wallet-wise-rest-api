package com.walletwise.service.impl;

import com.walletwise.domain.dto.ValidateValidationCodeRequest;
import com.walletwise.domain.entity.User;
import com.walletwise.domain.entity.UserValidationCode;
import com.walletwise.domain.exception.BusinessException;
import com.walletwise.gateway.Utils.CalculateDifferenceBetweenDates;
import com.walletwise.gateway.repository.IUserRepository;
import com.walletwise.gateway.repository.IUserValidationCodeRepository;
import com.walletwise.service.contract.IValidateValidationCodeService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
public class ValidateValidationCodeTests {
    private IValidateValidationCodeService service;
    @MockBean
    private IUserRepository repository;
    @MockBean
    private IUserValidationCodeRepository userValidationCodeRepository;
    @MockBean
    private PasswordEncoder encoder;
    @MockBean
    private CalculateDifferenceBetweenDates calculateDifferenceBetweenDates;

    @BeforeEach
    void setUp() {
        this.service = new ValidateValidationCodeService
                (repository, userValidationCodeRepository, encoder, calculateDifferenceBetweenDates);
    }

    @Test
    @DisplayName("Should throw business exception if the user does not exists")
    void shouldThrowBusinessExceptionIfUserDoesNotExists() {
        ValidateValidationCodeRequest request = mocks.validateValidationCodeRequestFactory();

        Mockito.when(repository.findByEmail(request.getEmail())).thenReturn(Optional.empty());

        Throwable exception = catchThrowable(() -> this.service.validate(request));

        Mockito.verify(repository, Mockito.times(1)).findByEmail(request.getEmail());
        assertThat(exception).isInstanceOf(BusinessException.class);
        assertThat(exception.getMessage()).isEqualTo("Can´t associate the email to any account.");
    }

    @Test
    @DisplayName("Should throw business exception if the user is active")
    void shouldThrowBusinessExceptionIfUserIsActive() {
        ValidateValidationCodeRequest request = mocks.validateValidationCodeRequestFactory();
        User savedUser = mocks.userFactory();
        savedUser.setActive(true);

        Mockito.when(repository.findByEmail(request.getEmail())).thenReturn(Optional.of(savedUser));

        Throwable exception = catchThrowable(() -> this.service.validate(request));

        Mockito.verify(repository, Mockito.times(1)).findByEmail(request.getEmail());
        assertThat(exception).isInstanceOf(BusinessException.class);
        assertThat(exception.getMessage()).isEqualTo("These account is already active. Please try to login with your account.");
    }

    @Test
    @DisplayName("Should throw business exception if the validation code is invalid")
    void shouldThrowBusinessExceptionIfTheValidationCodeIsInvalid() {
        ValidateValidationCodeRequest request = mocks.validateValidationCodeRequestFactory();
        User savedUser = mocks.userFactory();
        savedUser.setActive(false);

        Mockito.when(repository.findByEmail(request.getEmail())).thenReturn(Optional.of(savedUser));
        Mockito.when(userValidationCodeRepository.findByUserId(savedUser.getId())).thenReturn(Optional.empty());

        Throwable exception = catchThrowable(() -> this.service.validate(request));

        Mockito.verify(repository, Mockito.times(1)).findByEmail(request.getEmail());
        Mockito.verify(userValidationCodeRepository, Mockito.times(1)).findByUserId(savedUser.getId());
        assertThat(exception).isInstanceOf(BusinessException.class);
        assertThat(exception.getMessage()).isEqualTo("Invalid validation code. Please try again.");
    }

    @Test
    @DisplayName("Should throw business exception if the validation code dos not matches")
    void shouldThrowBusinessExceptionIfTheValidationCodeDoesNotMatches() {
        ValidateValidationCodeRequest request = mocks.validateValidationCodeRequestFactory();
        User savedUser = mocks.userFactory();
        savedUser.setActive(false);

        UserValidationCode savedUserValidationCode = mocks.userValidationCodeFactory(savedUser);

        Mockito.when(repository.findByEmail(request.getEmail())).thenReturn(Optional.of(savedUser));
        Mockito.when(userValidationCodeRepository.findByUserId(savedUser.getId())).thenReturn(Optional.of(savedUserValidationCode));
        Mockito.when(encoder.matches(request.getValidationCode(), savedUserValidationCode.getCode())).thenReturn(false);

        Throwable exception = catchThrowable(() -> this.service.validate(request));

        Mockito.verify(repository, Mockito.times(1)).findByEmail(request.getEmail());
        Mockito.verify(userValidationCodeRepository, Mockito.times(1)).findByUserId(savedUser.getId());
        Mockito.verify(encoder, Mockito.times(1)).matches(request.getValidationCode(), savedUserValidationCode.getCode());
        assertThat(exception).isInstanceOf(BusinessException.class);
        assertThat(exception.getMessage()).isEqualTo("Invalid validation code. Please try again.");
    }

    @Test
    @DisplayName("Should throw business exception if the validation has expired")
    void shouldThrowBusinessExceptionIfTheValidationCodeHasExpired() {
        ValidateValidationCodeRequest request = mocks.validateValidationCodeRequestFactory();
        User savedUser = mocks.userFactory();
        savedUser.setActive(false);

        UserValidationCode savedUserValidationCode = mocks.userValidationCodeFactory(savedUser);
        LocalDateTime aDateTime = LocalDateTime.of(2015, Month.AUGUST, 29, 19, 30, 40);
        savedUserValidationCode.setCreatedAt(aDateTime);
        Duration duration = Duration.ofMinutes(15);

        Mockito.when(repository.findByEmail(request.getEmail())).thenReturn(Optional.of(savedUser));
        Mockito.when(userValidationCodeRepository.findByUserId(savedUser.getId())).thenReturn(Optional.of(savedUserValidationCode));
        Mockito.when(encoder.matches(request.getValidationCode(), savedUserValidationCode.getCode())).thenReturn(true);
        Mockito.when(calculateDifferenceBetweenDates.calculate(Mockito.any(LocalDateTime.class), Mockito.any(LocalDateTime.class))).thenReturn(duration);

        Throwable exception = catchThrowable(() -> this.service.validate(request));

        Mockito.verify(repository, Mockito.times(1)).findByEmail(request.getEmail());
        Mockito.verify(userValidationCodeRepository, Mockito.times(1)).findByUserId(savedUser.getId());
        Mockito.verify(encoder, Mockito.times(1)).matches(request.getValidationCode(), savedUserValidationCode.getCode());
        Mockito.verify(calculateDifferenceBetweenDates, Mockito.times(1)).calculate(Mockito.any(LocalDateTime.class), Mockito.any(LocalDateTime.class));
        assertThat(exception).isInstanceOf(BusinessException.class);
        assertThat(exception.getMessage()).isEqualTo("The validation code you´ve entered has expired.");
    }

    @Test
    @DisplayName("Should change UserValidationCode status")
    void shouldChangeUserValidationCodeStatus() {
        ValidateValidationCodeRequest request = mocks.validateValidationCodeRequestFactory();
        User savedUser = mocks.userFactory();
        savedUser.setActive(false);

        UserValidationCode savedUserValidationCode = mocks.userValidationCodeFactory(savedUser);
        LocalDateTime aDateTime = LocalDateTime.of(2015, Month.AUGUST, 29, 19, 30, 40);
        savedUserValidationCode.setCreatedAt(aDateTime);
        Duration duration = Duration.ofMinutes(1);

        UserValidationCode toSaveUserValidationCode = mocks.userValidationCodeFactory(savedUser);
        toSaveUserValidationCode.setId(savedUserValidationCode.getId());
        toSaveUserValidationCode.setCode(savedUserValidationCode.getCode());
        toSaveUserValidationCode.setCreatedAt(aDateTime);
        toSaveUserValidationCode.setValidated(true);

        Mockito.when(repository.findByEmail(request.getEmail())).thenReturn(Optional.of(savedUser));
        Mockito.when(userValidationCodeRepository.findByUserId(savedUser.getId())).thenReturn(Optional.of(savedUserValidationCode));
        Mockito.when(encoder.matches(request.getValidationCode(), savedUserValidationCode.getCode())).thenReturn(true);
        Mockito.when(userValidationCodeRepository.save(toSaveUserValidationCode)).thenReturn(toSaveUserValidationCode);
        Mockito.when(calculateDifferenceBetweenDates.calculate(Mockito.any(LocalDateTime.class), Mockito.any(LocalDateTime.class))).thenReturn(duration);


        this.service.validate(request);

        Mockito.verify(repository, Mockito.times(1)).findByEmail(request.getEmail());
        Mockito.verify(userValidationCodeRepository, Mockito.times(1)).findByUserId(savedUser.getId());
        Mockito.verify(encoder, Mockito.times(1)).matches(request.getValidationCode(), savedUserValidationCode.getCode());
        Mockito.verify(userValidationCodeRepository, Mockito.times(1)).save(toSaveUserValidationCode);
        Mockito.verify(calculateDifferenceBetweenDates, Mockito.times(1)).calculate(Mockito.any(LocalDateTime.class), Mockito.any(LocalDateTime.class));
    }
}
