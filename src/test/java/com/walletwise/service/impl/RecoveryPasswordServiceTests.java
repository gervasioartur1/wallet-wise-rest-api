package com.walletwise.service.impl;

import com.walletwise.domain.dto.RecoveryPasswordRequest;
import com.walletwise.domain.dto.SendEmailRequest;
import com.walletwise.domain.entity.User;
import com.walletwise.domain.entity.UserValidationCode;
import com.walletwise.domain.exception.BusinessException;
import com.walletwise.gateway.Utils.GenerateRandomValidationCode;
import com.walletwise.gateway.helper.email.SendEmail;
import com.walletwise.gateway.repository.IUserRepository;
import com.walletwise.gateway.repository.IUserValidationCodeRepository;
import com.walletwise.service.contract.IRecoveryPasswordService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.rmi.UnexpectedException;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.catchThrowable;

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
class RecoveryPasswordServiceTests {
    private IRecoveryPasswordService service;
    @MockBean
    private IUserRepository repository;
    @MockBean
    private IUserValidationCodeRepository userValidationCodeRepository;
    @MockBean
    private GenerateRandomValidationCode validationCodeGenerator;
    @MockBean
    private SendEmail sendEmail;
    @MockBean
    private PasswordEncoder encoder;

    @BeforeEach
    void setUp() {
        this.service = new RecoveryPasswordService
                (repository, userValidationCodeRepository, validationCodeGenerator, sendEmail, encoder);
    }

    @Test
    @DisplayName("Should throw Business exception if the email is not associated with any account")
    void shouldThrowBusinessExceptionIfEmailIsNotAssociatedWithAnyAccount() {
        RecoveryPasswordRequest request = mocks.recoveryPasswordRequestFactory();

        Mockito.when(this.repository.findByEmail(request.getEmail())).thenReturn(Optional.empty());

        Throwable exception = catchThrowable(() -> this.service.recovery(request));
        Mockito.verify(this.repository, Mockito.times(1)).findByEmail(request.getEmail());
        Assertions.assertThat(exception).isInstanceOf(BusinessException.class);
        Assertions.assertThat(exception.getMessage()).isEqualTo("Could´t find your account, make sure you entered the correct credentials!");
    }

    @Test
    @DisplayName("Should save ValidationCode if not exits")
    void shouldSaveValidationCodeIfNotExits() throws UnexpectedException {
        RecoveryPasswordRequest request = mocks.recoveryPasswordRequestFactory();
        User savedUser = mocks.userFactory();
        savedUser.setActive(false);

        int generatedValidationCode = 654321;
        String notEncodeValidationCode = String.valueOf(generatedValidationCode);
        String encodedValidationCode = UUID.randomUUID().toString();

        UserValidationCode savedUservalidationCode = mocks.userValidationCodeFactory(savedUser);

        Mockito.when(this.repository.findByEmail(request.getEmail())).thenReturn(Optional.of(savedUser));
        Mockito.when(this.userValidationCodeRepository.findByUserId(savedUser.getId())).thenReturn(Optional.empty());
        Mockito.when(this.validationCodeGenerator.getValidationCode()).thenReturn(generatedValidationCode);
        Mockito.when(this.encoder.encode(notEncodeValidationCode)).thenReturn(encodedValidationCode);
        Mockito.when(this.userValidationCodeRepository.save(Mockito.any(UserValidationCode.class))).thenReturn(savedUservalidationCode);
        Mockito.when(this.repository.save(savedUser)).thenReturn(savedUser);

        this.service.recovery(request);

        Mockito.verify(this.repository, Mockito.times(1)).findByEmail(request.getEmail());
        Mockito.verify(this.userValidationCodeRepository, Mockito.times(1)).findByUserId(savedUser.getId());
        Mockito.verify(this.validationCodeGenerator, Mockito.times(1)).getValidationCode();
        Mockito.verify(this.encoder, Mockito.times(1)).encode(notEncodeValidationCode);
        Mockito.verify(this.userValidationCodeRepository).save(Mockito.any(UserValidationCode.class));
        Mockito.verify(this.repository, Mockito.times(1)).save(savedUser);
    }

    @Test
    @DisplayName("Should update ValidationCode if already exits")
    void shouldUpdateValidationCodeIfAlreadyExits() throws UnexpectedException {
        RecoveryPasswordRequest request = mocks.recoveryPasswordRequestFactory();
        User savedUser = mocks.userFactory();
        UserValidationCode savedUservalidationCode = mocks.userValidationCodeFactory(savedUser);

        int generatedValidationCode = 654321;
        String notEncodeValidationCode = String.valueOf(generatedValidationCode);
        String newEncodedValidationCode = UUID.randomUUID().toString();
        UserValidationCode toSaveUservalidationCode = mocks.toSaveUserValidationCodeFactory(savedUservalidationCode, newEncodedValidationCode);
        savedUser.setActive(false);

        Mockito.when(this.repository.findByEmail(request.getEmail())).thenReturn(Optional.of(savedUser));
        Mockito.when(this.userValidationCodeRepository.findByUserId(savedUser.getId())).thenReturn(Optional.of(savedUservalidationCode));
        Mockito.when(this.validationCodeGenerator.getValidationCode()).thenReturn(generatedValidationCode);
        Mockito.when(this.encoder.encode(notEncodeValidationCode)).thenReturn(newEncodedValidationCode);
        Mockito.when(this.userValidationCodeRepository.save(Mockito.any(UserValidationCode.class))).thenReturn(toSaveUservalidationCode);
        Mockito.when(this.repository.save(savedUser)).thenReturn(savedUser);


        this.service.recovery(request);

        Mockito.verify(this.repository, Mockito.times(1)).findByEmail(request.getEmail());
        Mockito.verify(this.userValidationCodeRepository, Mockito.times(1)).findByUserId(savedUser.getId());
        Mockito.verify(this.validationCodeGenerator, Mockito.times(1)).getValidationCode();
        Mockito.verify(this.encoder, Mockito.times(1)).encode(notEncodeValidationCode);
        Mockito.verify(this.userValidationCodeRepository).save(Mockito.any(UserValidationCode.class));
        Mockito.verify(this.repository, Mockito.times(1)).save(savedUser);
    }

    @Test
    @DisplayName("Should send email to user with validation code on save validation code success")
    void shouldSendEmailToUserWithValidationCodeOnSaveValidationCodeSuccess() throws UnexpectedException {
        RecoveryPasswordRequest request = mocks.recoveryPasswordRequestFactory();
        User savedUser = mocks.userFactory();

        int generatedValidationCode = 654321;
        String notEncodeValidationCode = String.valueOf(generatedValidationCode);
        String encodedValidationCode = UUID.randomUUID().toString();

        UserValidationCode savedUservalidationCode = mocks.userValidationCodeFactory(savedUser);
        savedUser.setActive(false);

        Mockito.when(this.repository.findByEmail(request.getEmail())).thenReturn(Optional.of(savedUser));
        Mockito.when(this.userValidationCodeRepository.findByUserId(savedUser.getId())).thenReturn(Optional.empty());
        Mockito.when(this.validationCodeGenerator.getValidationCode()).thenReturn(generatedValidationCode);
        Mockito.when(this.encoder.encode(notEncodeValidationCode)).thenReturn(encodedValidationCode);
        Mockito.when(this.userValidationCodeRepository.save(Mockito.any(UserValidationCode.class))).thenReturn(savedUservalidationCode);
        Mockito.when(this.repository.save(savedUser)).thenReturn(savedUser);
        Mockito.doNothing().when(this.sendEmail).send(Mockito.any(SendEmailRequest.class));

        this.service.recovery(request);

        Mockito.verify(this.repository, Mockito.times(1)).findByEmail(request.getEmail());
        Mockito.verify(this.userValidationCodeRepository, Mockito.times(1)).findByUserId(savedUser.getId());
        Mockito.verify(this.validationCodeGenerator, Mockito.times(1)).getValidationCode();
        Mockito.verify(this.encoder, Mockito.times(1)).encode(notEncodeValidationCode);
        Mockito.verify(this.userValidationCodeRepository).save(Mockito.any(UserValidationCode.class));
        Mockito.verify(this.repository, Mockito.times(1)).save(savedUser);
        Mockito.verify(this.sendEmail, Mockito.times(1)).send(Mockito.any(SendEmailRequest.class));
    }

    @Test
    @DisplayName("Should throw runtime exception if email can´t be sent ")
    void shouldThrowRuntimeExceptionIfEmailCantBeSend() throws UnexpectedException {
        RecoveryPasswordRequest request = mocks.recoveryPasswordRequestFactory();
        User savedUser = mocks.userFactory();

        int generatedValidationCode = 654321;
        String notEncodeValidationCode = String.valueOf(generatedValidationCode);
        String encodedValidationCode = UUID.randomUUID().toString();

        UserValidationCode savedUservalidationCode = mocks.userValidationCodeFactory(savedUser);
        savedUser.setActive(false);

        Mockito.when(this.repository.findByEmail(request.getEmail())).thenReturn(Optional.of(savedUser));
        Mockito.when(this.userValidationCodeRepository.findByUserId(savedUser.getId())).thenReturn(Optional.empty());
        Mockito.when(this.validationCodeGenerator.getValidationCode()).thenReturn(generatedValidationCode);
        Mockito.when(this.encoder.encode(notEncodeValidationCode)).thenReturn(encodedValidationCode);
        Mockito.when(this.userValidationCodeRepository.save(Mockito.any(UserValidationCode.class))).thenReturn(savedUservalidationCode);
        Mockito.when(this.repository.save(savedUser)).thenReturn(savedUser);
        Mockito.doThrow(RuntimeException.class).when(this.sendEmail).send(Mockito.any(SendEmailRequest.class));

        Throwable exception = catchThrowable(() -> this.service.recovery(request));

        Mockito.verify(this.repository, Mockito.times(1)).findByEmail(request.getEmail());
        Mockito.verify(this.userValidationCodeRepository, Mockito.times(1)).findByUserId(savedUser.getId());
        Mockito.verify(this.validationCodeGenerator, Mockito.times(1)).getValidationCode();
        Mockito.verify(this.encoder, Mockito.times(1)).encode(notEncodeValidationCode);
        Mockito.verify(this.userValidationCodeRepository).save(Mockito.any(UserValidationCode.class));
        Mockito.verify(this.repository, Mockito.times(1)).save(savedUser);
        Mockito.verify(this.sendEmail, Mockito.times(1)).send(Mockito.any(SendEmailRequest.class));
        Assertions.assertThat(exception).isInstanceOf(RuntimeException.class);
    }
}
