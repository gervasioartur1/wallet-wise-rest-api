package com.walletwise.service.impl;

import com.walletwise.domain.dto.CreateNewPasswordRequest;
import com.walletwise.domain.entity.User;
import com.walletwise.domain.entity.UserValidationCode;
import com.walletwise.domain.exception.BusinessException;
import com.walletwise.gateway.repository.IUserRepository;
import com.walletwise.gateway.repository.IUserValidationCodeRepository;
import com.walletwise.service.contract.ICreateNewPasswordService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;
import java.util.UUID;

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
class CreateNewPasswordServiceTests {
    private ICreateNewPasswordService service;
    @MockBean
    private IUserRepository repository;
    @MockBean
    private IUserValidationCodeRepository userValidationCodeRepository;
    @MockBean
    private PasswordEncoder encoder;

    @BeforeEach
    void setUp() {
        this.service = new CreateNewPasswordService
                (repository, userValidationCodeRepository, encoder);
    }

    @Test
    @DisplayName("Should throw business exception if the user does not exists")
    void shouldThrowBusinessExceptionIfUserDoesNotExists() {
        CreateNewPasswordRequest request = mocks.createNewPasswordRequestFactory();

        Mockito.when(repository.findByEmail(request.getEmail())).thenReturn(Optional.empty());

        Throwable exception = Assertions.catchThrowable(() -> this.service.create(request));

        Mockito.verify(repository, Mockito.times(1)).findByEmail(request.getEmail());
        Assertions.assertThat(exception).isInstanceOf(BusinessException.class);
        Assertions.assertThat(exception.getMessage()).isEqualTo("Can´t associate the email to any account.");
    }

    @Test
    @DisplayName("Should throw business exception if the user validation does not exists")
    void shouldThrowBusinessExceptionIfUserValidationDoesNotExists() {
        CreateNewPasswordRequest request = mocks.createNewPasswordRequestFactory();
        User saved = mocks.savedUserFactory();

        Mockito.when(repository.findByEmail(request.getEmail())).thenReturn(Optional.of(saved));
        Mockito.when(userValidationCodeRepository.findByUserId(saved.getId())).thenReturn(Optional.empty());

        Throwable exception = Assertions.catchThrowable(() -> this.service.create(request));

        Mockito.verify(repository, Mockito.times(1)).findByEmail(request.getEmail());
        Mockito.verify(userValidationCodeRepository, Mockito.times(1)).findByUserId(saved.getId());
        Assertions.assertThat(exception).isInstanceOf(BusinessException.class);
        Assertions.assertThat(exception.getMessage()).isEqualTo("Can´t process the information, make sure you´ve reset you password to user this page.");
    }

    @Test
    @DisplayName("Should throw business exception if the user validation situation is false")
    void shouldThrowBusinessExceptionIfUserValidationSituationIsFalse() {
        CreateNewPasswordRequest request = mocks.createNewPasswordRequestFactory();
        User saved = mocks.savedUserFactory();
        UserValidationCode savedUserValidationCode = mocks.savedUserValidationCodeFactory(saved, false);

        Mockito.when(repository.findByEmail(request.getEmail())).thenReturn(Optional.of(saved));
        Mockito.when(userValidationCodeRepository.findByUserId(saved.getId())).thenReturn(Optional.of(savedUserValidationCode));

        Throwable exception = Assertions.catchThrowable(() -> this.service.create(request));

        Mockito.verify(repository, Mockito.times(1)).findByEmail(request.getEmail());
        Mockito.verify(userValidationCodeRepository, Mockito.times(1)).findByUserId(saved.getId());
        Assertions.assertThat(exception).isInstanceOf(BusinessException.class);
        Assertions.assertThat(exception.getMessage()).isEqualTo("Please,Validate the code send on your email to create a new password!");
    }

    @Test
    @DisplayName("Should update the password of the user with new encoded password")
    void shouldUpdateThePasswordOfTheUserWithNewEncodedPassword() {
        CreateNewPasswordRequest request = mocks.createNewPasswordRequestFactory();
        User saved = mocks.savedUserFactory();

        String newEncodedPassword = UUID.randomUUID().toString();
        saved.setPassword(newEncodedPassword);

        UserValidationCode savedUserValidationCode = mocks.savedUserValidationCodeFactory(saved, true);

        Mockito.when(repository.findByEmail(request.getEmail())).thenReturn(Optional.of(saved));
        Mockito.when(userValidationCodeRepository.findByUserId(saved.getId())).thenReturn(Optional.of(savedUserValidationCode));
        Mockito.when(encoder.encode(request.getPassword())).thenReturn(newEncodedPassword);
        Mockito.when(repository.save(saved)).thenReturn(saved);

        this.service.create(request);

        Mockito.verify(repository, Mockito.times(1)).findByEmail(request.getEmail());
        Mockito.verify(userValidationCodeRepository, Mockito.times(1)).findByUserId(saved.getId());
        Mockito.verify(encoder, Mockito.times(1)).encode(request.getPassword());
        Mockito.verify(repository, Mockito.times(1)).save(saved);
    }

    @Test
    @DisplayName("Should delete the userValidationCode data on success")
    void shouldDeleteTheUserValidationCodeDataOnSuccess() {
        CreateNewPasswordRequest request = mocks.createNewPasswordRequestFactory();
        User saved = mocks.savedUserFactory();

        String newEncodedPassword = UUID.randomUUID().toString();
        saved.setPassword(newEncodedPassword);

        UserValidationCode savedUserValidationCode = mocks.savedUserValidationCodeFactory(saved, true);

        Mockito.when(repository.findByEmail(request.getEmail())).thenReturn(Optional.of(saved));
        Mockito.when(userValidationCodeRepository.findByUserId(saved.getId())).thenReturn(Optional.of(savedUserValidationCode));
        Mockito.when(encoder.encode(request.getPassword())).thenReturn(newEncodedPassword);
        Mockito.when(repository.save(saved)).thenReturn(saved);
        Mockito.doNothing().when(userValidationCodeRepository).deleteById(savedUserValidationCode.getId());

        this.service.create(request);

        Mockito.verify(repository, Mockito.times(1)).findByEmail(request.getEmail());
        Mockito.verify(userValidationCodeRepository, Mockito.times(1)).findByUserId(saved.getId());
        Mockito.verify(encoder, Mockito.times(1)).encode(request.getPassword());
        Mockito.verify(repository, Mockito.times(1)).save(saved);
        Mockito.verify(userValidationCodeRepository, Mockito.times(1)).deleteById(savedUserValidationCode.getId());
    }
}
