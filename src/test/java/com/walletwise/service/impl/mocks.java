package com.walletwise.service.impl;

import com.walletwise.domain.dto.*;
import com.walletwise.domain.entity.User;
import com.walletwise.domain.entity.UserValidationCode;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

public class mocks {
    public static Authentication authenticationFactory(boolean isAuthenticated) {
        return new Authentication() {
            @Override
            public Collection<? extends GrantedAuthority> getAuthorities() {
                return List.of();
            }

            @Override
            public Object getCredentials() {
                return null;
            }

            @Override
            public Object getDetails() {
                return null;
            }

            @Override
            public Object getPrincipal() {
                return null;
            }

            @Override
            public boolean isAuthenticated() {
                return isAuthenticated;
            }

            @Override
            public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {

            }

            @Override
            public String getName() {
                return "";
            }
        };
    }

    public static User userFactory() {
        return User
                .builder()
                .id(UUID.randomUUID())
                .firstName("any_first_name")
                .lastName("any_last_name")
                .username("any_username")
                .email("any_email")
                .password("any_password")
                .active(true)
                .role("any_role")
                .build();
    }

    public static User savedUserFactory() {
        return User
                .builder()
                .id(UUID.randomUUID())
                .firstName("any_first_name")
                .lastName("any_last_name")
                .username("any_username")
                .email("any_email")
                .password(UUID.randomUUID().toString())
                .active(true)
                .role("any_role")
                .build();
    }

    public static User toSaveUserFactory() {
        return User
                .builder()
                .firstName("any_first_name")
                .lastName("any_last_name")
                .username("any_username")
                .email("any_email")
                .password("any_password")
                .active(true)
                .role("USER")
                .build();
    }

    public static UserValidationCode userValidationCodeFactory(User savedUser) {
        String encodedValidationCode = UUID.randomUUID().toString();
        return UserValidationCode
                .builder()
                .id(UUID.randomUUID())
                .userId(savedUser.getId())
                .code(encodedValidationCode)
                .build();
    }


    public static UserValidationCode savedUserValidationCodeFactory(User savedUser, boolean situation) {
        String encodedValidationCode = UUID.randomUUID().toString();
        LocalDateTime aDateTime = LocalDateTime.of(2015, Month.AUGUST, 29, 19, 30, 40);
        return UserValidationCode
                .builder()
                .id(UUID.randomUUID())
                .userId(savedUser.getId())
                .code(encodedValidationCode)
                .createdAt(aDateTime)
                .validated(situation)
                .build();
    }

    public static UserValidationCode toSaveUserValidationCodeFactory(UserValidationCode savedUservalidationCode, String newEncodedValidationCode) {
        LocalDateTime aDateTime = LocalDateTime.of(2015, Month.AUGUST, 29, 19, 30, 40);
        return UserValidationCode
                .builder()
                .id(savedUservalidationCode.getId())
                .userId(savedUservalidationCode.getUserId())
                .code(newEncodedValidationCode)
                .createdAt(aDateTime)
                .build();
    }

    public static CreateUserRequest createUserRequestFactory() {
        return CreateUserRequest
                .builder()
                .firstName("any_first_name")
                .lastName("any_last_name")
                .username("any_username")
                .email("any_email")
                .password("any_password")
                .build();
    }

    public static LoginRequest loginRequestFactory() {
        return LoginRequest
                .builder()
                .usernameOrEmail("gervasio@gmail.com")
                .password("any_password")
                .build();
    }

    public static RecoveryPasswordRequest recoveryPasswordRequestFactory() {
        return RecoveryPasswordRequest
                .builder()
                .email("any_email")
                .build();
    }

    public static ValidateValidationCodeRequest validateValidationCodeRequestFactory() {
        return ValidateValidationCodeRequest
                .builder()
                .email("any_email")
                .validationCode("any_validation_code")
                .build();
    }

    public static CreateNewPasswordRequest createNewPasswordRequestFactory() {
        return CreateNewPasswordRequest
                .builder()
                .email("any_email")
                .password("any_password")
                .build();
    }
}
