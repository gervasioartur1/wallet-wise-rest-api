package com.walletwise.service.impl;

import com.walletwise.domain.dto.LoginRequest;
import com.walletwise.domain.entity.User;
import com.walletwise.domain.exception.BusinessException;
import com.walletwise.domain.model.UserAccount;
import com.walletwise.gateway.helper.token.GenerateToken;
import com.walletwise.gateway.repository.IUserRepository;
import com.walletwise.service.contract.ILoginService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Mockito.*;

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
class LoginServiceTests {
    private ILoginService service;
    @MockBean
    private IUserRepository repository;
    @MockBean
    private AuthenticationManager authenticationManager;
    @MockBean
    private GenerateToken generateToken;

    @BeforeEach
    void setUp() {
        this.service = new LoginService(repository, authenticationManager, generateToken);
    }

    @Test
    @DisplayName("Should throw businessException if email does not exists")
    void shouldThrowBusinessExceptionIfEmailDoesNotExists() {
        LoginRequest request = mocks.loginRequestFactory();

        when(this.repository.findByEmail(request.getUsernameOrEmail())).thenReturn(Optional.empty());

        Throwable exception = catchThrowable(() -> this.service.login(request));
        verify(this.repository, times(1)).findByEmail(request.getUsernameOrEmail());
        verify(this.repository, times(0)).findByUsername(request.getUsernameOrEmail());
        assertThat(exception).isInstanceOf(BusinessException.class);
        assertThat(exception.getMessage()).isEqualTo("The email: " + request.getUsernameOrEmail() + ", is not associated with any account! Try again with correct credentials or Registry yourself.");
    }

    @Test
    @DisplayName("should throw Business exception if username does not exists")
    void shouldThrowBusinessExceptionIfUsernameDoesNotExists() {
        LoginRequest request = mocks.loginRequestFactory();
        request.setUsernameOrEmail("gervasio");

        when(this.repository.findByUsername(request.getUsernameOrEmail())).thenReturn(Optional.empty());

        Throwable exception = catchThrowable(() -> this.service.login(request));
        verify(this.repository, times(1)).findByUsername(request.getUsernameOrEmail());
        verify(this.repository, times(0)).findByEmail(request.getUsernameOrEmail());
        assertThat(exception).isInstanceOf(BusinessException.class);
        assertThat(exception.getMessage()).isEqualTo("The username: " + request.getUsernameOrEmail() + ", is not associated with any account! Try again with correct credentials or Registry yourself.");
    }

    @Test
    @DisplayName("Should return Business exception if user account is not activated")
    void shouldReturnBusinessExceptionIfAccountIsNotActivated() {
        LoginRequest request = mocks.loginRequestFactory();
        User savedUser = mocks.toSaveUserFactory();
        savedUser.setActive(false);

        when(this.repository.findByEmail(request.getUsernameOrEmail())).thenReturn(Optional.of(savedUser));

        Throwable exception = catchThrowable(() -> this.service.login(request));

        verify(this.repository, times(0)).findByUsername(request.getUsernameOrEmail());
        verify(this.repository, times(1)).findByEmail(request.getUsernameOrEmail());
        assertThat(exception).isInstanceOf(BusinessException.class);
        assertThat(exception.getMessage()).isEqualTo("The account is not activated! please verify your email get instructions to active tour account.");
    }

    @Test
    @DisplayName("Should authenticate throw Business exception if the password is not correct")
    void ShouldThrowBusinessExceptionIfThePasswordIsNotCorrect() {
        Authentication authentication = mocks.authenticationFactory(false);
        LoginRequest request = mocks.loginRequestFactory();
        User savedUser = mocks.toSaveUserFactory();

        when(this.repository.findByEmail(request.getUsernameOrEmail())).thenReturn(Optional.of(savedUser));
        when(this.authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(savedUser.getUsername(), request.getPassword()))).thenReturn(authentication);

        Throwable exception = catchThrowable(() -> this.service.login(request));

        verify(this.repository, times(0)).findByUsername(request.getUsernameOrEmail());
        verify(this.repository, times(1)).findByEmail(request.getUsernameOrEmail());
        verify(this.authenticationManager, times(1)).authenticate(new UsernamePasswordAuthenticationToken(savedUser.getUsername(), request.getPassword()));
        assertThat(exception).isInstanceOf(BusinessException.class);
        assertThat(exception.getMessage()).isEqualTo("Invalid username or password!");
    }

    @Test
    @DisplayName("Should return the token on authentication success")
    void ShouldReturnTheTokenOnAuthenticationSuccess() {
        Authentication authentication = mocks.authenticationFactory(true);
        LoginRequest request = mocks.loginRequestFactory();
        String token = UUID.randomUUID().toString();
        User savedUser = mocks.toSaveUserFactory();


        when(this.repository.findByEmail(request.getUsernameOrEmail())).thenReturn(Optional.of(savedUser));
        when(this.authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(savedUser.getUsername(), request.getPassword()))).thenReturn(authentication);
        when(generateToken.generate(savedUser.getUsername())).thenReturn(token);

        UserAccount userAccount = this.service.login(request);

        verify(this.repository, times(0)).findByUsername(request.getUsernameOrEmail());
        verify(this.repository, times(1)).findByEmail(request.getUsernameOrEmail());
        verify(this.authenticationManager, times(1)).authenticate(new UsernamePasswordAuthenticationToken(savedUser.getUsername(), request.getPassword()));
        verify(generateToken, times(1)).generate(savedUser.getUsername());
        assertThat(userAccount.getToken()).isEqualTo(token);
    }
}
