package com.walletwise.api.resource;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.walletwise.domain.dto.CreateNewPasswordRequest;
import com.walletwise.domain.exception.BusinessException;
import com.walletwise.service.contract.ICreateNewPasswordService;
import com.walletwise.service.impl.mocks;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.context.WebApplicationContext;

import static org.mockito.BDDMockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@WebMvcTest(CreateNewPasswordController.class)
@ExtendWith(SpringExtension.class)
class CreateNewPasswordControllerTests {
    private final String AUTH_CREATE_NEW_PASSWORD = "/auth/create_new_password";
    @Autowired
    MockMvc mvc;
    @MockBean
    ICreateNewPasswordService service;

    @Autowired
    private WebApplicationContext context;


    @BeforeEach
    public void setup() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .build();
    }


    @Test
    @DisplayName("Should return badRequest if email is empty")
    void shouldReturnBadRequestIfEmailIsEmpty() throws Exception {
        CreateNewPasswordRequest requestParams = mocks.createNewPasswordRequestFactory();
        requestParams.setEmail("");

        String json = new ObjectMapper().writeValueAsString(requestParams);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(AUTH_CREATE_NEW_PASSWORD)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("body", Matchers.is("The field 'email' is required!")));

        verify(service, times(0)).create(requestParams);
    }

    @Test
    @DisplayName("Should return badRequest if email is Null")
    void shouldReturnBadRequestIfEmailIsNull() throws Exception {
        CreateNewPasswordRequest requestParams = mocks.createNewPasswordRequestFactory();
        requestParams.setEmail(null);

        String json = new ObjectMapper().writeValueAsString(requestParams);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(AUTH_CREATE_NEW_PASSWORD)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("body", Matchers.is("The field 'email' is required!")));

        verify(service, times(0)).create(requestParams);
    }

    @Test
    @DisplayName("Should return badRequest if email is invalid")
    void shouldReturnBadRequestIfEmailIsInvalid() throws Exception {
        CreateNewPasswordRequest requestParams = mocks.createNewPasswordRequestFactory();
        String json = new ObjectMapper().writeValueAsString(requestParams);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(AUTH_CREATE_NEW_PASSWORD)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("body", Matchers.is("The field 'email' is invalid!")));

        verify(service, times(0)).create(requestParams);
    }

    @Test
    @DisplayName("Should return badRequest if password is empty")
    void shouldReturnBadRequestIfPasswordIsEmpty() throws Exception {
        CreateNewPasswordRequest requestParams = mocks.createNewPasswordRequestFactory();
        requestParams.setEmail("gervasio@gmail.com");
        requestParams.setPassword("");

        String json = new ObjectMapper().writeValueAsString(requestParams);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(AUTH_CREATE_NEW_PASSWORD)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("body", Matchers.is("The field 'password' is required!")));

        verify(service, times(0)).create(requestParams);
    }

    @Test
    @DisplayName("Should return badRequest if password is Null")
    void shouldReturnBadRequestIfPasswordIsNull() throws Exception {
        CreateNewPasswordRequest requestParams = mocks.createNewPasswordRequestFactory();
        requestParams.setEmail("gervasio@gmail.com");
        requestParams.setPassword(null);

        String json = new ObjectMapper().writeValueAsString(requestParams);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(AUTH_CREATE_NEW_PASSWORD)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("body", Matchers.is("The field 'password' is required!")));

        verify(service, times(0)).create(requestParams);
    }


    @Test
    @DisplayName("Should return badRequest if password is not strong")
    void shouldReturnBadRequestIfPasswordIsNotStrong() throws Exception {
        CreateNewPasswordRequest requestParams = mocks.createNewPasswordRequestFactory();
        requestParams.setEmail("gervasio@gmail.com");
        requestParams.setPassword("123456");

        String json = new ObjectMapper().writeValueAsString(requestParams);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(AUTH_CREATE_NEW_PASSWORD)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("body", Matchers.is("The 'password' must have minimum 6 and maximum 32 characters, at least one uppercase letter, one lowercase letter, one number and one special character!")));

        verify(service, times(0)).create(requestParams);
    }


    @Test
    @DisplayName("Should throw business exception if the email does not exists")
    void shouldThrowBusinessExceptionIfTheEmailDoesExists() throws Exception {
        CreateNewPasswordRequest requestParams = mocks.createNewPasswordRequestFactory();
        requestParams.setEmail("gervasio@gmail.com");
        requestParams.setPassword("Gervasio@123");

        String json = new ObjectMapper().writeValueAsString(requestParams);

        doThrow(new BusinessException("Can´t associate the email to any account.")).when(service).create(requestParams);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(AUTH_CREATE_NEW_PASSWORD)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("body", Matchers.is("Can´t associate the email to any account.")));

        verify(service, times(1)).create(requestParams);
    }

    @Test
    @DisplayName("Should throw business exception if the UserValidationCode dos not exists")
    void shouldThrowBusinessExceptionIfTheUserValidationCodeDoesNotExists() throws Exception {
        CreateNewPasswordRequest requestParams = mocks.createNewPasswordRequestFactory();
        requestParams.setEmail("gervasio@gmail.com");
        requestParams.setPassword("Gervasio@123");

        String json = new ObjectMapper().writeValueAsString(requestParams);

        doThrow(new BusinessException("Can´t process the information, make sure you´ve reset you password to user this page.")).when(service).create(requestParams);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(AUTH_CREATE_NEW_PASSWORD)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("body", Matchers.is("Can´t process the information, make sure you´ve reset you password to user this page.")));

        verify(service, times(1)).create(requestParams);
    }

    @Test
    @DisplayName("Should throw business exception the UserValidationCode status is false")
    void shouldThrowBusinessExceptionIfTheUserValidationCodeStatusIsFalse() throws Exception {
        CreateNewPasswordRequest requestParams = mocks.createNewPasswordRequestFactory();
        requestParams.setEmail("gervasio@gmail.com");
        requestParams.setPassword("Gervasio@123");

        String json = new ObjectMapper().writeValueAsString(requestParams);

        doThrow(new BusinessException("Please,Validate the code send on your email to create a new password!")).when(service).create(requestParams);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(AUTH_CREATE_NEW_PASSWORD)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("body", Matchers.is("Please,Validate the code send on your email to create a new password!")));

        verify(service, times(1)).create(requestParams);
    }

    @Test
    @DisplayName("Should throw unexpected exception if the the service throws")
    void shouldThrowUnexpectedException() throws Exception {
        CreateNewPasswordRequest requestParams = mocks.createNewPasswordRequestFactory();
        requestParams.setEmail("gervasio@gmail.com");
        requestParams.setPassword("Gervasio@123");

        String json = new ObjectMapper().writeValueAsString(requestParams);

        doThrow(HttpServerErrorException.InternalServerError.class).when(service).create(requestParams);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(AUTH_CREATE_NEW_PASSWORD)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isInternalServerError());

        verify(service, times(1)).create(requestParams);
    }

    @Test
    @DisplayName("Should return user account on success ")
    void shouldReturnUserAccountOnSuccess() throws Exception {
        CreateNewPasswordRequest requestParams = mocks.createNewPasswordRequestFactory();
        requestParams.setEmail("gervasio@gmail.com");
        requestParams.setPassword("Gervasio@123");

        String json = new ObjectMapper().writeValueAsString(requestParams);

        doNothing().when(service).create(requestParams);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(AUTH_CREATE_NEW_PASSWORD)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isOk())
                .andExpect(jsonPath("body").value("Password successfully recovered. Please sing in with your new credentials!"));

        verify(service, times(1)).create(requestParams);
    }
}
