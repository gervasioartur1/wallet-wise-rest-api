package com.walletwise.api.resource;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.walletwise.domain.dto.RecoveryPasswordRequest;
import com.walletwise.domain.exception.BusinessException;
import com.walletwise.service.contract.IRecoveryPasswordService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.context.WebApplicationContext;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@WebMvcTest(RecoveryPasswordController.class)
public class RecoveryPasswordControllerTests {
    private final String RECOVERY_PASSWORD_API = "/auth/recovery_password";
    @Autowired
    MockMvc mvc;
    @MockBean
    IRecoveryPasswordService service;

    @Autowired
    private WebApplicationContext context;


    @BeforeEach
    public void setup() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .build();
    }

    @Test
    @DisplayName("Should return badRequest if usernameOrEmail is empty")
    void shouldReturnBadRequestIfUsernameOrEmailIsEmpty() throws Exception {
        RecoveryPasswordRequest requestParams = RecoveryPasswordRequest.builder().email("").build();
        String json = new ObjectMapper().writeValueAsString(requestParams);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(RECOVERY_PASSWORD_API)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("body", Matchers.is("The field 'Username or Email' is required!")));

        verify(service, times(0)).recovery(requestParams);
    }

    @Test
    @DisplayName("Should return badRequest if usernameOrEmail is null")
    void shouldReturnBadRequestIfUsernameOrEmailIsNull() throws Exception {
        RecoveryPasswordRequest requestParams = RecoveryPasswordRequest.builder().email(null).build();
        String json = new ObjectMapper().writeValueAsString(requestParams);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(RECOVERY_PASSWORD_API)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("body", Matchers.is("The field 'Username or Email' is required!")));

        verify(service, times(0)).recovery(requestParams);
    }

    @Test
    @DisplayName("Should return badRequest if the user email account can´t be find")
    void shouldReturnBadRequestITheUserAccountCantBeFind() throws Exception {
        RecoveryPasswordRequest requestParams = RecoveryPasswordRequest
                .builder()
                .email("any_email")
                .build();

        String json = new ObjectMapper().writeValueAsString(requestParams);
        doThrow(new BusinessException("Could´t find your account, make sure you entered the correct credentials!")).when(this.service).recovery(requestParams);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(RECOVERY_PASSWORD_API)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("body", Matchers.is("Could´t find your account, make sure you entered the correct credentials!")));

        verify(service, times(1)).recovery(requestParams);
    }

    @Test
    @DisplayName("Should throw unexpected exception if the the service throws")
    void shouldThrowUnexpectedException() throws Exception {
        RecoveryPasswordRequest requestParams = RecoveryPasswordRequest
                .builder()
                .email("any_email")
                .build();

        String json = new ObjectMapper().writeValueAsString(requestParams);
        doThrow(HttpServerErrorException.InternalServerError.class).when(this.service).recovery(requestParams);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(RECOVERY_PASSWORD_API)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isInternalServerError());
        verify(service, times(1)).recovery(requestParams);

    }

    @Test
    @DisplayName("Should return user account on success ")
    void shouldReturnUserAccountOnSuccess() throws Exception {
        RecoveryPasswordRequest requestParams = RecoveryPasswordRequest
                .builder()
                .email("any_email")
                .build();

        String json = new ObjectMapper().writeValueAsString(requestParams);
        doNothing().when(service).recovery(requestParams);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(RECOVERY_PASSWORD_API)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isOk())
                .andExpect(jsonPath("body")
                        .value("We´ve send instruction to recopy your password in the email : " + requestParams.getEmail()));

        verify(service, times(1)).recovery(requestParams);
    }
}
