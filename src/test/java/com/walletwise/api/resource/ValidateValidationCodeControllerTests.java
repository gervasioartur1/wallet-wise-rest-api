package com.walletwise.api.resource;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.walletwise.domain.dto.ValidateValidationCodeRequest;
import com.walletwise.domain.exception.BusinessException;
import com.walletwise.service.contract.IValidateValidationCodeService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.context.WebApplicationContext;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@WebMvcTest(ValidateValidationCodeController.class)
public class ValidateValidationCodeControllerTests {
    private final String VALIDATE_VALIDATION_CODE = "/auth/validate_validation_code";
    @Autowired
    MockMvc mvc;
    @MockBean
    IValidateValidationCodeService service;

    @Autowired
    private WebApplicationContext context;


    @BeforeEach
    public void setup() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .build();
    }

    @Test
    @DisplayName("Should return badRequest if email is empty")
    void shouldReturnBadRequestIfEmailIsEmpty() throws Exception {
        ValidateValidationCodeRequest requestParams = ValidateValidationCodeRequest.builder().email("").build();
        String json = new ObjectMapper().writeValueAsString(requestParams);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(VALIDATE_VALIDATION_CODE)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("body", Matchers.is("The field 'email' is required!")));

        verify(service, times(0)).validate(requestParams);
    }

    @Test
    @DisplayName("Should return badRequest if Email is null")
    void shouldReturnBadRequestIfEmailIsNull() throws Exception {
        ValidateValidationCodeRequest requestParams = ValidateValidationCodeRequest.builder().email("").build();
        String json = new ObjectMapper().writeValueAsString(requestParams);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(VALIDATE_VALIDATION_CODE)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("body", Matchers.is("The field 'email' is required!")));

        verify(service, times(0)).validate(requestParams);
    }

    @Test
    @DisplayName("Should return badRequest if email is invalid")
    void shouldReturnBadRequestIfEmailIsInvalid() throws Exception {
        ValidateValidationCodeRequest requestParams = ValidateValidationCodeRequest
                .builder()
                .email("any_email")
                .build();

        String json = new ObjectMapper().writeValueAsString(requestParams);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(VALIDATE_VALIDATION_CODE)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("body", Matchers.is("The field 'email' is invalid!")));

        verify(service, times(0)).validate(requestParams);
    }

    @Test
    @DisplayName("Should return badRequest if  validationCode is empty")
    void shouldReturnBadRequestIfValidationCodeEmpty() throws Exception {
        ValidateValidationCodeRequest requestParams = ValidateValidationCodeRequest
                .builder()
                .email("gervasio@gmail.com")
                .validationCode("")
                .build();
        String json = new ObjectMapper().writeValueAsString(requestParams);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(VALIDATE_VALIDATION_CODE)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("body", Matchers.is("The field 'validationCode' is required!")));

        verify(service, times(0)).validate(requestParams);
    }

    @Test
    @DisplayName("Should return badRequest if validationCode is null")
    void shouldReturnBadRequestIfValidationCodeIsNull() throws Exception {
        ValidateValidationCodeRequest requestParams = ValidateValidationCodeRequest
                .builder()
                .email("gervasio@gmail.com")
                .validationCode(null)
                .build();
        String json = new ObjectMapper().writeValueAsString(requestParams);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(VALIDATE_VALIDATION_CODE)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("body", Matchers.is("The field 'validationCode' is required!")));

        verify(service, times(0)).validate(requestParams);
    }

    @Test
    @DisplayName("Should return badRequest if  user is not registered")
    void shouldReturnBadRequestIfUserIsNotRegistered() throws Exception {
        ValidateValidationCodeRequest requestParams = ValidateValidationCodeRequest
                .builder()
                .email("gervasio@gmail.com")
                .validationCode("123456")
                .build();
        String json = new ObjectMapper().writeValueAsString(requestParams);
        doThrow(new BusinessException("Can´t associate the email to any account.")).when(this.service).validate(requestParams);


        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(VALIDATE_VALIDATION_CODE)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("body", Matchers.is("Can´t associate the email to any account.")));

        verify(service, times(1)).validate(requestParams);
    }

    @Test
    @DisplayName("Should return badRequest if  user account is activated")
    void shouldReturnBadRequestIfUserAccountIsActivated() throws Exception {
        ValidateValidationCodeRequest requestParams = ValidateValidationCodeRequest
                .builder()
                .email("gervasio@gmail.com")
                .validationCode("123456")
                .build();
        String json = new ObjectMapper().writeValueAsString(requestParams);
        doThrow(new BusinessException("These account is already active. Please try to login with your account.")).when(this.service).validate(requestParams);


        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(VALIDATE_VALIDATION_CODE)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("body", Matchers.is("These account is already active. Please try to login with your account.")));

        verify(service, times(1)).validate(requestParams);
    }

    @Test
    @DisplayName("Should return badRequest if  validation code is invalid")
    void shouldReturnBadRequestIValidationCodeIsInvalid() throws Exception {
        ValidateValidationCodeRequest requestParams = ValidateValidationCodeRequest
                .builder()
                .email("gervasio@gmail.com")
                .validationCode("123456")
                .build();
        String json = new ObjectMapper().writeValueAsString(requestParams);
        doThrow(new BusinessException("Invalid validation code. Please try again.")).when(this.service).validate(requestParams);


        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(VALIDATE_VALIDATION_CODE)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("body", Matchers.is("Invalid validation code. Please try again.")));

        verify(service, times(1)).validate(requestParams);
    }

    @Test
    @DisplayName("Should return badRequest if  validation code has expired")
    void shouldReturnBadRequestIValidationCodeHasExpired() throws Exception {
        ValidateValidationCodeRequest requestParams = ValidateValidationCodeRequest
                .builder()
                .email("gervasio@gmail.com")
                .validationCode("123456")
                .build();
        String json = new ObjectMapper().writeValueAsString(requestParams);
        doThrow(new BusinessException("The validation code you´ve entered has expired.")).when(this.service).validate(requestParams);


        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(VALIDATE_VALIDATION_CODE)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("body", Matchers.is("The validation code you´ve entered has expired.")));

        verify(service, times(1)).validate(requestParams);
    }

    @Test
    @DisplayName("Should return Unexpected exception when service throws")
    void shouldReturnUnexpectedWhenServiceThrows() throws Exception {
        ValidateValidationCodeRequest requestParams = ValidateValidationCodeRequest
                .builder()
                .email("gervasio@gmail.com")
                .validationCode("123456")
                .build();
        String json = new ObjectMapper().writeValueAsString(requestParams);
        doThrow(HttpServerErrorException.InternalServerError.class).when(this.service).validate(requestParams);


        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(VALIDATE_VALIDATION_CODE)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isInternalServerError());
        verify(service, times(1)).validate(requestParams);
    }

    @Test
    @DisplayName("Should return success message  on success")
    void shouldReturnSuccessMessageOnSuccess() throws Exception {
        ValidateValidationCodeRequest requestParams = ValidateValidationCodeRequest
                .builder()
                .email("gervasio@gmail.com")
                .validationCode("123456")
                .build();
        String json = new ObjectMapper().writeValueAsString(requestParams);
        doNothing().when(this.service).validate(requestParams);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(VALIDATE_VALIDATION_CODE)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isOk())
                .andExpect(jsonPath("body", Matchers.is("Validation code successfully validated.")));

        verify(service, times(1)).validate(requestParams);
    }
}
