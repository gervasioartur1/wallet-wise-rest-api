package com.walletwise.api.resource;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.walletwise.domain.dto.LoginRequest;
import com.walletwise.domain.exception.BusinessException;
import com.walletwise.domain.model.UserAccount;
import com.walletwise.service.contract.ILoginService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.context.WebApplicationContext;

import java.util.UUID;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@WebMvcTest(LoginController.class)
@ExtendWith(SpringExtension.class)
public class LoginControllerTests {
    private final String LOGIN_API = "/auth/login";
    @Autowired
    MockMvc mvc;
    @MockBean
    ILoginService service;

    @Autowired
    private WebApplicationContext context;


    @BeforeEach
    public void setup() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .build();
    }

    @Test
    @DisplayName("Should return badRequest if usernameOrEmail is empty")
    void shouldReturnBadRequestIfUsernameOrEmailIsEmpty() throws Exception {
        LoginRequest requestParams = LoginRequest.builder().usernameOrEmail("").build();
        String json = new ObjectMapper().writeValueAsString(requestParams);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(LOGIN_API)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("body", Matchers.is("The field 'Username or Email' is required!")));

        verify(service, times(0)).login(requestParams);
    }

    @Test
    @DisplayName("Should return badRequest if usernameOrEmail is null")
    void shouldReturnBadRequestIfUsernameOrEmailIsNull() throws Exception {
        LoginRequest requestParams = LoginRequest.builder().usernameOrEmail(null).build();
        String json = new ObjectMapper().writeValueAsString(requestParams);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(LOGIN_API)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("body", Matchers.is("The field 'Username or Email' is required!")));

        verify(service, times(0)).login(requestParams);
    }

    @Test
    @DisplayName("Should return badRequest if password is empty")
    void shouldReturnBadRequestIfPasswordIsEmpty() throws Exception {
        LoginRequest requestParams = LoginRequest
                .builder()
                .usernameOrEmail("any_email")
                .password("")
                .build();
        String json = new ObjectMapper().writeValueAsString(requestParams);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(LOGIN_API)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("body", Matchers.is("The field 'password' is required!")));

        verify(service, times(0)).login(requestParams);
    }

    @Test
    @DisplayName("Should return badRequest if password is null")
    void shouldReturnBadRequestIfPasswordIsNull() throws Exception {
        LoginRequest requestParams = LoginRequest
                .builder()
                .usernameOrEmail("any_email")
                .password(null)
                .build();
        String json = new ObjectMapper().writeValueAsString(requestParams);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(LOGIN_API)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("body", Matchers.is("The field 'password' is required!")));

        verify(service, times(0)).login(requestParams);
    }

    @Test
    @DisplayName("Should return badRequest if the user email does not exits")
    void shouldReturnBadRequestITheUserEmailDoesNotExits() throws Exception {
        LoginRequest requestParams = LoginRequest
                .builder()
                .usernameOrEmail("any_email")
                .password("any_password")
                .build();
        String json = new ObjectMapper().writeValueAsString(requestParams);
        doThrow(new BusinessException("The email: " + requestParams.getUsernameOrEmail() + ", is not associated with any account! Try again with correct credentials or Registry yourself.")).when(this.service).login(requestParams);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(LOGIN_API)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("body", Matchers.is("The email: " + requestParams.getUsernameOrEmail() + ", is not associated with any account! Try again with correct credentials or Registry yourself.")));

        verify(service, times(1)).login(requestParams);
    }

    @Test
    @DisplayName("Should return badRequest if the username does not exits")
    void shouldReturnBadRequestITheUsernameDoesNotExits() throws Exception {
        LoginRequest requestParams = LoginRequest
                .builder()
                .usernameOrEmail("any_username")
                .password("any_password")
                .build();
        String json = new ObjectMapper().writeValueAsString(requestParams);
        doThrow(new BusinessException("The username: " + requestParams.getUsernameOrEmail() + ", is not associated with any account! Try again with correct credentials or Registry yourself.")).when(this.service).login(requestParams);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(LOGIN_API)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("body", Matchers.is("The username: " + requestParams.getUsernameOrEmail() + ", is not associated with any account! Try again with correct credentials or Registry yourself.")));

        verify(service, times(1)).login(requestParams);
    }

    @Test
    @DisplayName("Should return badRequest if the user account is not activated")
    void shouldReturnBadRequestITheUserAccountIsNotActivated() throws Exception {
        LoginRequest requestParams = LoginRequest
                .builder()
                .usernameOrEmail("any_username")
                .password("any_password")
                .build();
        String json = new ObjectMapper().writeValueAsString(requestParams);
        doThrow(new BusinessException("The account is not activated! please verify your email get instructions to active tour account.")).when(this.service).login(requestParams);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(LOGIN_API)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("body", Matchers.is("The account is not activated! please verify your email get instructions to active tour account.")));

        verify(service, times(1)).login(requestParams);
    }

    @Test
    @DisplayName("Should return badRequest if the user credentials are invalid")
    void shouldReturnBadRequestITheUserCredentialsAreInvalid() throws Exception {
        LoginRequest requestParams = LoginRequest
                .builder()
                .usernameOrEmail("any_username")
                .password("any_password")
                .build();
        String json = new ObjectMapper().writeValueAsString(requestParams);
        doThrow(new BusinessException("Invalid username or password!")).when(this.service).login(requestParams);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(LOGIN_API)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("body", Matchers.is("Invalid username or password!")));

        verify(service, times(1)).login(requestParams);
    }

    @Test
    @DisplayName("Should throw unexpected exception if the the service throws")
    void shouldThrowUnexpectedException() throws Exception {
        LoginRequest requestParams = LoginRequest
                .builder()
                .usernameOrEmail("any_username")
                .password("any_password")
                .build();
        String json = new ObjectMapper().writeValueAsString(requestParams);
        doThrow(HttpServerErrorException.InternalServerError.class).when(this.service).login(requestParams);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(LOGIN_API)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isInternalServerError());
        verify(service, times(1)).login(requestParams);

    }

    @Test
    @DisplayName("Should return user account on success ")
    void shouldReturnUserAccountOnSuccess() throws Exception {
        LoginRequest requestParams = LoginRequest
                .builder()
                .usernameOrEmail("any_username")
                .password("any_password")
                .build();

        String json = new ObjectMapper().writeValueAsString(requestParams);
        UserAccount userAccount = UserAccount.builder().token(UUID.randomUUID().toString()).build();
        given(this.service.login(requestParams)).willReturn(userAccount);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(LOGIN_API)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isOk())
                .andExpect(jsonPath("body.token").value(userAccount.getToken()));

        verify(service, times(1)).login(requestParams);
    }
}
