package com.walletwise.api.resource;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.walletwise.domain.dto.CreateUserRequest;
import com.walletwise.domain.exception.BusinessException;
import com.walletwise.domain.model.UserAccount;
import com.walletwise.service.contract.ICreateUserService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.context.WebApplicationContext;

import java.util.UUID;

import static org.mockito.BDDMockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@WebMvcTest(CreateUserController.class)
@ExtendWith(SpringExtension.class)
class CreateUserControllerTests {
    private final String USER_API = "/users";
    @Autowired
    MockMvc mvc;
    @MockBean
    ICreateUserService userService;

    @Autowired
    private WebApplicationContext context;


    @BeforeEach
    public void setup() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .build();
    }


    @Test
    @DisplayName("Should return badRequest if firstName is empty")
    void shouldReturnBadRequestIfFirstNameIsEmpty() throws Exception {
        CreateUserRequest requestParams = CreateUserRequest.builder().firstName("").build();
        String json = new ObjectMapper().writeValueAsString(requestParams);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(USER_API + "/create_user")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("body", Matchers.is("The field 'First name' is required!")));

        verify(userService, times(0)).create(requestParams);
    }

    @Test
    @DisplayName("Should return badRequest if firstName is null")
    void shouldReturnBadRequestIfFirstNameIsNull() throws Exception {
        CreateUserRequest requestParams = CreateUserRequest.builder().firstName(null).build();
        String json = new ObjectMapper().writeValueAsString(requestParams);


        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(USER_API + "/create_user")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("body", Matchers.is("The field 'First name' is required!")));

        verify(userService, times(0)).create(requestParams);
    }


    @Test
    @DisplayName("Should return badRequest if lastName is empty")
    void shouldReturnBadRequestIfLatNameIsEmpty() throws Exception {
        CreateUserRequest requestParams = CreateUserRequest.builder().firstName("Joao Henrique").lastName("").build();
        String json = new ObjectMapper().writeValueAsString(requestParams);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(USER_API + "/create_user")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("body", Matchers.is("The field 'Last name' is required!")));

        verify(userService, times(0)).create(requestParams);
    }

    @Test
    @DisplayName("Should return badRequest if lastName is Null")
    void shouldReturnBadRequestIfLatNameIsNull() throws Exception {
        CreateUserRequest requestParams = CreateUserRequest.builder().firstName("Joao Henrique").lastName(null).build();
        String json = new ObjectMapper().writeValueAsString(requestParams);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(USER_API + "/create_user")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("body", Matchers.is("The field 'Last name' is required!")));

        verify(userService, times(0)).create(requestParams);
    }


    @Test
    @DisplayName("Should return badRequest if username is empty")
    void shouldReturnBadRequestIfUserNameIsEmpty() throws Exception {
        CreateUserRequest requestParams = CreateUserRequest
                .builder()
                .firstName("Joao Henrique")
                .lastName("Campos")
                .username("")
                .build();


        String json = new ObjectMapper().writeValueAsString(requestParams);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(USER_API + "/create_user")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("body", Matchers.is("The field 'username' is required!")));

        verify(userService, times(0)).create(requestParams);
    }

    @Test
    @DisplayName("Should return badRequest if username is Null")
    void shouldReturnBadRequestIfUserNameIsNull() throws Exception {
        CreateUserRequest requestParams = CreateUserRequest
                .builder()
                .firstName("Joao Henrique")
                .lastName("Campos")
                .username(null)
                .build();
        String json = new ObjectMapper().writeValueAsString(requestParams);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(USER_API + "/create_user")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("body", Matchers.is("The field 'username' is required!")));

        verify(userService, times(0)).create(requestParams);
    }

    @Test
    @DisplayName("Should return badRequest if username is invalid")
    void shouldReturnBadRequestIfUserNameIsInvalid() throws Exception {
        CreateUserRequest requestParams = CreateUserRequest
                .builder()
                .firstName("Joao Henrique")
                .lastName("Campos")
                .username("@gervasio")
                .build();
        String json = new ObjectMapper().writeValueAsString(requestParams);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(USER_API + "/create_user")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("body", Matchers.is("The username should not contain the character '@'.")));

        verify(userService, times(0)).create(requestParams);
    }

    @Test
    @DisplayName("Should return badRequest if email is empty")
    void shouldReturnBadRequestIfEmailIsEmpty() throws Exception {
        CreateUserRequest requestParams = CreateUserRequest
                .builder()
                .firstName("Joao Henrique")
                .lastName("Campos")
                .username("camposjoao")
                .email("")
                .build();


        String json = new ObjectMapper().writeValueAsString(requestParams);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(USER_API + "/create_user")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("body", Matchers.is("The field 'email' is required!")));

        verify(userService, times(0)).create(requestParams);
    }

    @Test
    @DisplayName("Should return badRequest if email is Null")
    void shouldReturnBadRequestIfEmailIsNull() throws Exception {
        CreateUserRequest requestParams = CreateUserRequest
                .builder()
                .firstName("Joao Henrique")
                .lastName("Campos")
                .username("camposjoao")
                .email(null)
                .build();
        String json = new ObjectMapper().writeValueAsString(requestParams);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(USER_API + "/create_user")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("body", Matchers.is("The field 'email' is required!")));

        verify(userService, times(0)).create(requestParams);
    }

    @Test
    @DisplayName("Should return badRequest if email is invalid")
    void shouldReturnBadRequestIfEmailIsInvalid() throws Exception {
        CreateUserRequest requestParams = CreateUserRequest
                .builder()
                .firstName("Joao Henrique")
                .lastName("Campos")
                .username("camposjoao")
                .email("any_invalid_email")
                .build();
        String json = new ObjectMapper().writeValueAsString(requestParams);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(USER_API + "/create_user")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("body", Matchers.is("The field 'email' is invalid!")));

        verify(userService, times(0)).create(requestParams);
    }

    @Test
    @DisplayName("Should return badRequest if password is empty")
    void shouldReturnBadRequestIfPasswordIsEmpty() throws Exception {
        CreateUserRequest requestParams = CreateUserRequest
                .builder()
                .firstName("Joao Henrique")
                .lastName("Campos")
                .username("camposjoao")
                .email("gervasio@gmail.com")
                .password("")
                .build();


        String json = new ObjectMapper().writeValueAsString(requestParams);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(USER_API + "/create_user")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("body", Matchers.is("The field 'password' is required!")));

        verify(userService, times(0)).create(requestParams);
    }

    @Test
    @DisplayName("Should return badRequest if password is Null")
    void shouldReturnBadRequestIfPasswordIsNull() throws Exception {
        CreateUserRequest requestParams = CreateUserRequest
                .builder()
                .firstName("Joao Henrique")
                .lastName("Campos")
                .username("camposjoao")
                .email("gervasio@gmail.com")
                .password(null)
                .build();
        String json = new ObjectMapper().writeValueAsString(requestParams);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(USER_API + "/create_user")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("body", Matchers.is("The field 'password' is required!")));

        verify(userService, times(0)).create(requestParams);
    }


    @Test
    @DisplayName("Should return badRequest if password is not strong")
    void shouldReturnBadRequestIfPasswordIsNotStrong() throws Exception {
        CreateUserRequest requestParams = CreateUserRequest
                .builder()
                .firstName("Joao Henrique")
                .lastName("Campos")
                .username("camposjoao")
                .email("gervasio@gmail.com")
                .password("1234567890123")
                .build();
        String json = new ObjectMapper().writeValueAsString(requestParams);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(USER_API + "/create_user")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("body", Matchers.is("The 'password' must have minimum 6 and maximum 32 characters, at least one uppercase letter, one lowercase letter, one number and one special character!")));

        verify(userService, times(0)).create(requestParams);
    }

    @Test
    @DisplayName("Should throw business exception if email is already taken")
    void shouldThrowBusinessExceptionIfEmailIsTaken() throws Exception {
        CreateUserRequest requestParams = CreateUserRequest
                .builder()
                .firstName("Joao Henrique")
                .lastName("Campos")
                .username("camposjoao")
                .email("gervasio@gmail.com")
                .password("Integraldecosxdx@0199")
                .build();

        String json = new ObjectMapper().writeValueAsString(requestParams);
        String errorMessage = "We've found an account with email: " + requestParams.getEmail() + "! Try to login to the account.";
        given(userService.create(requestParams)).willThrow(new BusinessException(errorMessage));

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(USER_API + "/create_user")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("body", Matchers.is(errorMessage)));

        verify(userService, times(1)).create(requestParams);
    }


    @Test
    @DisplayName("Should throw business exception if usernam is already taken")
    void shouldThrowBusinessExceptionIfUsernameIsTaken() throws Exception {
        CreateUserRequest requestParams = CreateUserRequest
                .builder()
                .firstName("Joao Henrique")
                .lastName("Campos")
                .username("camposjoao")
                .email("gervasio@gmail.com")
                .password("Integraldecosxdx@0199")
                .build();

        String json = new ObjectMapper().writeValueAsString(requestParams);
        String errorMessage = "We've found an account with username: " + requestParams.getUsername() + "! Try to login to the account.";
        given(userService.create(requestParams)).willThrow(new BusinessException(errorMessage));

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(USER_API + "/create_user")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("body", Matchers.is(errorMessage)));

        verify(userService, times(1)).create(requestParams);
    }

    @Test
    @DisplayName("Should throw unexpected exception if the the service throws")
    void shouldThrowUnexpectedException() throws Exception {
        CreateUserRequest requestParams = CreateUserRequest
                .builder()
                .firstName("Joao Henrique")
                .lastName("Campos")
                .username("camposjoao")
                .email("gervasio@gmail.com")
                .password("Integraldecosxdx@0199")
                .build();

        String json = new ObjectMapper().writeValueAsString(requestParams);
        doThrow(HttpServerErrorException.InternalServerError.class).when(userService).create(requestParams);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(USER_API + "/create_user")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isInternalServerError());

        verify(userService, times(1)).create(requestParams);
    }

    @Test
    @DisplayName("Should return user account on success ")
    void shouldReturnUserAccountOnSuccess() throws Exception {
        CreateUserRequest requestParams = CreateUserRequest
                .builder()
                .firstName("Joao Henrique")
                .lastName("Campos")
                .username("camposjoao")
                .email("gervasio@gmail.com")
                .password("Integraldecosxdx@0199")
                .build();

        String json = new ObjectMapper().writeValueAsString(requestParams);
        UserAccount userAccount = UserAccount.builder().token(UUID.randomUUID().toString()).build();
        given(userService.create(requestParams)).willReturn(userAccount);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(USER_API + "/create_user")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mvc
                .perform(request)
                .andExpect(status().isCreated())
                .andExpect(jsonPath("body.token").value(userAccount.getToken()));

        verify(userService, times(1)).create(requestParams);
    }
}
