package com.walletwise.api.validation.validators;

import com.walletwise.api.validation.validator.UsernameFieldValidator;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
public class UsernameFieldValidatorTests {
    @Test
    @DisplayName("Should return error message is the username is invalid")
    void shouldReturnErrorMessageIsTheUsernameIsInvalid() {
        String fieldValue = "@gervasio";

        UsernameFieldValidator validator = new UsernameFieldValidator(fieldValue);
        String result = validator.validate();
        assertThat(result).isEqualTo("The username should not contain the character '@'.");
    }

    @Test
    @DisplayName("Should return null if the username is valid")
    void shouldReturnNullIfTheUsernameIsValid() {
        Object fieldValue = "gervasio#";

        UsernameFieldValidator validator = new UsernameFieldValidator(fieldValue);
        String result = validator.validate();
        assertThat(result).isNull();
    }
}
