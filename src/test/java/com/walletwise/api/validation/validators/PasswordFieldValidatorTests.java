package com.walletwise.api.validation.validators;

import com.walletwise.api.validation.validator.PasswordFieldValidator;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
class PasswordFieldValidatorTests {
    @Test
    @DisplayName("Should return error message if password field is not string")
    void shouldReturnErrorMessageIfPasswordFieldIsNotString() {
        String fieldName = "password";
        String fieldValue = "123456";

        PasswordFieldValidator validator = new PasswordFieldValidator(fieldName, fieldValue);
        String result = validator.validate();
        assertThat(result).isEqualTo("The 'password' must have minimum 6 and maximum 32 characters, at least one uppercase letter, one lowercase letter, one number and one special character!");
    }

    @Test
    @DisplayName("Should return null if password is strong")
    public void shouldReturnNullIfPasswordIsString() {
        String fieldName = "password";
        String fieldValue = "Integraldecosxdx@0199";

        PasswordFieldValidator validator = new PasswordFieldValidator(fieldName, fieldValue);
        String result = validator.validate();
        assertThat(result).isNull();
    }

}
